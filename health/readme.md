# Health-related data

See also covid folder for covid-related data


## Alcohol disorders

Share of males and females, suffering from alcohol use
disorders. Alcohol dependence is defined by the
International Classification of Diseases as the presence of three or
more indicators of dependence for at least a month within the
previous year. This is given as the age-standardized prevalence which
assumes a constant age structure allowing for comparison by sex,
country and through time. 

IHME, Global Burden of Disease Study (2019) – processed by Our World
in Data.  
[Dowloaded from OWiD](https://ourworldindata.org/alcohol-consumption)

Variables:

* **Entity**: country, region, continent...
* **Code**: ISO3 country code
* **Year**
* **disordersM**: Current number of cases of alcohol use disorders per 100 people, in
  males, age-standardized
* **disordersF**: Current number of cases of alcohol use disorders per 100 people, in
  females, age-standardized,
* **population**: Population (historical estimates),
* **Continent**: largely missing



## heart

Downloaded from
[kaggle](https://www.kaggle.com/rashikrahmanpritom/heart-attack-analysis-prediction-dataset?select=heart.csv) 

* Age : Age of the patient
* Sex : Sex of the patient
* exang: exercise induced angina (1 = yes; 0 = no)
* ca: number of major vessels (0-3)
* cp : Chest Pain type chest pain type
     Value 1: typical angina
     Value 2: atypical angina
     Value 3: non-anginal pain
     Value 4: asymptomatic
* trtbps : resting blood pressure (in mm Hg)
* chol : cholestoral in mg/dl fetched via BMI sensor
* fbs : (fasting blood sugar > 120 mg/dl) (1 = true; 0 = false)
* rest_ecg : resting electrocardiographic results
     Value 0: normal
     Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV)
     Value 2: showing probable or definite left ventricular hypertrophy by Estes' criteria
* thalach : maximum heart rate achieved
* target : 0= less chance of heart attack 1= more chance of heart attack


## howell-height-weight

Downloaded from from [rethinking
package](https://github.com/rmcelreath/rethinking/blob/master/data/Howell2.csv),
but originates from University of Toronto [datasets
collection](https://tspace.library.utoronto.ca/handle/1807/10395).
The data is collected by Nancy Howell in 1960s, it
contains a number of different samples, e.g. adults only, adult men
only, kids only, some snowball sampling...  So it is not a
representative dataset.


## ISS: International Smoking Statistics

_ISS-Germany_, _ISS-Sweden_, _ISS-USA_: downloaded from [International
Smoking Statistics](http://www.pnlee.co.uk/ISS.htm).  The page claims
that

     This work is copyright. It may be reproduced or quoted in whole or in part for study or research purposes, subject to inclusion of an acknowledgement of the source. It may not be reproduced for purposes other than those above or sold without written permission from the authors.

Hence the data may probably not copied to other repos.


## Kluender (2021) medical debt

Data from [eTable
 8](https://cdn.jamanetwork.com/ama/content_public/journal/jama/938737/joi210060supp1_prod_1626283681.69756.pdf?Expires=1678992581&Signature=4nFf51VjPoV0gTOIr-DvEBvPh~yMiatQ~PEGDA4cS2KsHlvsmVCRIWSji7fA1P-wyRm1uWyH8ZCu0H5EVZHWh2JL2HlM~w8fUOBiHhPJuKz~G51ZfatZFrMob2lxiuRt8H-w18MYUgsHcbtAUNgfZ8WUTYxKguAaZgjyyVRsn5XtQS9jZCRlhWapM8erbcVllepWgnsAUkMu6EDs~RYXoAE9urxr9nPqQPJP893nmc66FjRFGk3iOLzIF0Cbh3LG5RjD8U401a~KaZmc5g71ZTyTsINKIOFoa0RVVhrGd9vBoXJ~MghOMwt~D0mgj3BQ8tJJfrFyO585VSNV5OU4eQ__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA)
in 

> Kluender, Mahoney, Wong and Yin: _Medical Dept in the US, 2009-2020_,
> JAMA 326(3), pp 250-256 (2021).

Average medical debt in three groups of US states.  Observation is _state
group--year--debt type_.

Note: this is _unconditional_ debt--it includes those who do not have
medical debt.

* **expansion**: state medicaid expansion status (expanded 2014, after
  2014, did not expand)
* **type**: medical debt, non-medical debt
* **year**: 2009-2020
* **debt**: mean medical debt (2020 dollars) for all credit reports.
  


## Lung cancer males

Downloaded from [WHO Cancer
Database](https://www-dep.iarc.fr/WHOdb/WHOdb.htm):

* **Year** (1950 is the earliest)
* **Deaths**
* **Crude rate** (number of deaths per 100,000 population at risk)
* **ASR (W)** age standardized rate, per 100,000
* **Cumulative risk** missing


## ncbirths

Can be downloaded from
[Openintro webpage](https://www.openintro.org/data/index.php?data=ncbirths)

In 2004, the state of North Carolina released to the public a
large data set containing information on births recorded in this
state. This data set has been of interest to medical researchers
who are studying the relation between habits and practices of
expectant mothers and the birth of their children. This is a
random sample of 1,000 cases from this data set.

A data frame with 1000 observations on the following 13 variables.

Variables:

* **fage** Father's age in years.
* **mage** Mother's age in years.
* **mature** Maturity status of mother.
* **weeks** Length of pregnancy in weeks.
* **premie** Whether the birth was classified as premature (premie) or
     full-term.
* **visits** Number of hospital visits during pregnancy.
* **gained** Weight gained by mother during pregnancy in pounds.
* **weight** Weight of the baby at birth in pounds.
* **lowbirthweight** Whether baby was classified as low birthweight
     (‘low’) or not (‘not low’).
* **gender** Gender of the baby, ‘female’ or ‘male’.
* **habit** Status of the mother as a ‘nonsmoker’ or a ‘smoker’.
* **marital** Whether mother is ‘married’ or ‘not married’ at birth.
* **whitemom** Whether mom is ‘white’ or ‘not white’.



## Right Heart Catheterization Dataset

http://biostat.mc.vanderbilt.edu/wiki/pub/Main/DataSets/rhc.html

This dataset was used in Connors et al. (1996): The effectiveness of
RHC in the initial care of critically ill patients. J American Medical
Association 276:889-897. The dataset pertains to day 1 of
hospitalization, i.e., the "treatment" variable swang1 is whether or
not a patient received a RHC (also called the Swan-Ganz catheter) on
the first day in which the patient qualified for the SUPPORT study
(see above). The dataset is suitable for use in papers submitted in
response to the call for papers on causal inference, by the journal
Health Services and Outcomes Research Methodology. The original
analysis by Connors et al. used binary logistic model to develop a
propensity score that was then used for matching RHC patients with
non-RHC patients. A sensitivity analysis was also done. The results
provided some evidence that patients receiving RHC had decreased
survival time, and the sensitivity analysis indicated that any
unmeasured confounder would have to be somewhat strong to explain away
the results. See Lin DY, Psaty BM, Kronmal RA (1998): Assessing the
sensitivity of regression results to unmeasured confounders in
observational studies. Biometrics 54:948-963 for useful methods for
sensitivity analysis, one of which was applied to the RHC results.

### Variables

#### Demographics

* Age: Age
* Sex: Sex
* Race: Race
* Edu: Years of education
* Income: Income
* Ninsclas: Medical insurance
* Cat1: Primary disease category
* Cat2: Secondary disease category

== Categories of admission diagnosis ==
Resp: Respiratory Diagnosis
Card: Cardiovascular Diagnosis
Neuro: Neurological Diagnosis
Gastr: Gastrointestinal Diagnosis
Renal: Renal Diagnosis
Meta: Metabolic Diagnosis
Hema: Hematologic Diagnosis
Seps: Sepsis Diagnosis
Trauma: Trauma Diagnosis
Ortho: Orthopedic Diagnosis  	 

== ==
adld3p: adl
Das2d3pc: DASI ( Duke Activity Status Index)
Dnr1: DNR status on day1
Ca: Cancer
Surv2md1: Support model estimate of the prob. of surviving 2 months
Aps1: APACHE score
Scoma1: Glasgow Coma Score
Wtkilo1: Weight
Temp1: Temperature
Meanbp1: Mean blood pressure
Resp1: Respiratory rate
Hrt1: Heart rate
Pafi1: PaO2/FIO2 ratio
Paco21: PaCo2
Ph1: PH
Wblc1: WBC
Hema1: Hematocrit
Sod1: Sodium
Pot1: Potassium
Crea1: Creatinine
Bili1: Bilirubin
Alb1: Albumin
Urin1: Urine output

== Categories of comorbidities illness==
Cardiohx: Acute MI, Peripheral Vascular Disease, Severe Cardiovascular Symptoms (NYHA-Class III), Very Severe Cardiovascular Symptoms (NYHA-Class IV)
Chfhx: Congestive Heart Failure
Dementhx: Dementia, Stroke or Cerebral Infarct, Parkinson’s Disease
Psychhx: Psychiatric History, Active Psychosis or Severe Depression
Chrpulhx: Chronic Pulmonary Disease, Severe Pulmonary Disease, Very Severe Pulmonary Disease
Renalhx: Chronic Renal Disease, Chronic Hemodialysis or Peritoneal Dialysis
Liverhx: Cirrhosis, Hepatic Failure
Gibledhx: Upper GI Bleeding
Malighx: Solid Tumor, Metastatic Disease, Chronic Leukemia/Myeloma, Acute Leukemia, Lymphoma
Immunhx: Immunosupperssion, Organ Transplant, HIV Positivity, Diabetes Mellitus Without End Organ Damage, Diabetes Mellitus With End Organ Damage, Connective Tissue Disease
Transhx: Transfer (> 24 Hours) from Another Hospital
Amihx: Definite Myocardial Infarction

== ==
Swang1: Right Heart Catheterization (RHC)
Sadmdte: Study Admission Date
Dthdte: Date of Death
Lstctdte: Date of Last Contact
Dschdte: Hospital Discharge Date
Death: Death at any time up to 180 Days
Ptid: Patient ID


## wdbc: Wisconsin Diagnostic Breast Cancer

Downloaded from [UCI ML Repository](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic))
569 cases, data is from November 1995.

### Variables

1. ID number
2. Diagnosis (M = malignant, B = benign).  This is what you normally
   predict. 
3. -- 32: ten real-valued features computed for each cell nucleus.
   For each feature the mean, standard error, and "worst" or largest (mean of the three
   largest values) of these features were computed for each image,
   resulting in 30 features.  For instance, field 3 is Mean Radius, field
   13 is Radius SE, field 23 is Worst Radius. 
   
   It contains the following features:

   1. radius (mean of distances from center to points on the perimeter)
   2. texture (standard deviation of gray-scale values)
   3. perimeter
   4. area
   5. smoothness (local variation in radius lengths)
   6. compactness (perimeter^2 / area - 1.0)
   7. concavity (severity of concave portions of the contour)
   8. concave points (number of concave portions of the contour)
   9. symmetry 
   10. fractal dimension ("coastline approximation" - 1)

See the references at [UCI ML
Repository](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic))
the for more details.
