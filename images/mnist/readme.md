# mnist digits

Original files (download from http://yann.lecun.com/exdb/mnist/):

* t10k-labels-idx1-ubyte.gz
* train-labels-idx1-ubyte.gz
* t10k-images-idx3-ubyte.gz
* train-images-idx3-ubyte.gz
