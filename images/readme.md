# Image data

## mnist: mnist digits

## Cats-n-dogs

Kaggle cats and dogs, 25k labelled training images, 12.5k unlabeled
test images.

## Squares-circles

* _squares-circles.R_: code to create images of squares and circles
* _squares-circles/_: image files, "ci" denotes circles, "sq" denotes
  squares.

## Visualizations

A small subset from [Leilani Battle's
  webpate](https://www.cs.umd.edu/~leilani/beagle.html).
These are 929 small data visualizations, $399\times 711$ pixels, 
scraped from the web, mostly
low quality.

## Printed text in different languages

* language-text-images: various printed texts in DA, EN, RU, TH, ZN.
* `book-page-images.sh`: make images of pages of a single text source
* `image-all-books.sh`: create images of all books in a given folder.
