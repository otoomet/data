#!/usr/bin/bash
## Convert all utf-8 books into page images using book-page-images.sh
## choose the font according to the language

VERBOSE=1
OUTPUTDIR=language-text-images
SPLITLINES=10
POINTSIZE=19

while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
      -f|--force)
	  FORCE=1
	  shift
	  ;;
      -h|--help)
  	  echo "usage: $0 [--force]"
	  echo "convert all books in books/ folder into page images"
	  echo "store images in 'language-text-images'"
	  echo "choose suitable font according to the input language"
	  echo "  -f, --force     : force the output dir to be overwritten"
	  echo "  -l, --lines <lines>: split texts into how many lines (default $SPLITLINES)"
	  echo "  -v <v>, --verbose: verbosity leve (default $VERBOSE)"
  	  exit 0
  	  ;;
      -l|--lines)
	  SPLITLINES="$2"
	  shift; shift
	  ;;
      --pointsize)
	  POINTSIZE="$2"
	  shift
	  shift
	  ;;
      -v|--verbose)
	  VERBOSE="$2"
	  shift; shift
	  ;;
  esac
done
echo "split into ${SPLITLINES}-lines chunks"
echo "use ${POINTSIZE}pt font"

folders=("books/" "law/"
	 "ai-for-thai-best/novel/"
	)
languages=()
books=""
for folder in ${folders[@]}; do
    books="${books} $(ls $folder)"
done

for book in ${books} ; do
    language=${book%%.*}
    language=${language##*_}
    languages+=($language)
done
echo ${#languages[@]} "books"
languages=$(echo "${languages[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' ')
echo "unique languages:" ${languages}

if [ -d $OUTPUTDIR ]; then
    if [ -z $FORCE ]; then
	echo "cannot proceed -- directory ${OUTPUTDIR} exists"
	echo "use --force to overwrite"
	exit 1
    fi
    if [ $VERBOSE -gt 0 ]; then
	echo "deleting ${OUTPUTDIR} for new content"
    fi
    rm -rf $OUTPUTDIR/*
else
    mkdir $OUTPUTDIR || exit 1
fi

for language in ${languages} ; do
    echo $language
    THAICORPUS=""
    NOHEADERS=""
    case $language in
	"ZN")
	    font="Noto-Sans-CJK-HK"
	    WRAP=35
	    ;;
	"TH")
	    font="Noto-Serif-Thai-Regular"
	    WRAP=60
	    THAICORPUS="--thai-corpus"
	    NOHEADERS="--no-headers"
	    ;;
	*)
	    font="liberation-serif"
	    WRAP=60
	    ;;
    esac
    books=""
    for folder in ${folders[@]}; do
	books="${books} $(ls ${folder}*_${language}.* 2>/dev/null)"
    done
    for book in ${books} ; do
	if [ $VERBOSE -gt 0 ]; then
	    echo $book
	fi
	## Headers: no gutenberg headers for Thai corpus and ZN law
	HEADERS=""
	dirname=$(dirname ${book})
	dirname1=${dirname%%/*}
	if [ $dirname1 == "law" ] || [ $dirname1 == "ai-for-thai-best" ]; then
	    HEADERS="--no-headers"
	fi
	if [ $VERBOSE -gt 1 ]; then
	    echo $font
	fi
	./book-page-images.sh --force --verbose 0 --wrap $WRAP\
			      --lines $SPLITLINES\
			      --pointsize $POINTSIZE --font $font\
			      $HEADERS\
			      $THAICORPUS\
			      $book &&\
	    mv book-page-images/* $OUTPUTDIR
    done
done

## training-validation split
echo "training-validation split"
n=$(ls $OUTPUTDIR|wc -l)
nValidation=$(($n/5))
if [ $VERBOSE -gt 0 ]; then
    echo "in all $n image files, $nValidation validation files"
fi
validationimages=$(find $OUTPUTDIR -name "*.jpg" -print | sort -R | head -$nValidation)
echo $(echo $validationimages | wc -l) "validation images"
mkdir $OUTPUTDIR/validation
mkdir $OUTPUTDIR/train
## use xargs as there are too many files here
# mv $validation ${OUTPUTDIR}/validation
echo "move validation files to" ${OUTPUTDIR}/validation/
echo $validationimages | xargs echo "mv -t ${OUTPUTDIR}/validation/"
echo $validationimages | xargs mv -t ${OUTPUTDIR}/validation/
echo "done"
echo $(ls ${OUTPUTDIR}/validation/ |wc -l) output files
find $OUTPUTDIR -maxdepth 1 -name '*.jpg' -print | xargs mv -t ${OUTPUTDIR}/train/

echo "cropping"
for trainType in "validation" "train"; do
    echo $traintype
    mkdir $OUTPUTDIR/${trainType}-cropped
    for f in $OUTPUTDIR/$trainType/*.jpg; do
	bf=$(basename $f);
	convert $OUTPUTDIR/${trainType}/$bf -gravity Center -crop 256x256+0+0 +repage $OUTPUTDIR/${trainType}-cropped/$bf
    done
done
echo "All done :-)"
