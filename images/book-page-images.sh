#!/usr/bin/bash
## Convert utf-8 texts into page images
## remove the Project Gutenberg heades and footers
## Based on  Scarlett Hwang's work

MAGICK=convert

POSITIONAL=()
SPLITLINES=22  # lines per page (LINES interferes with screen?)
FONT="Noto-Sans-CJK-HK"
# fonts that work:
#   RU: liberation-serif
#   ZN: Noto-Sans-CJK-HK
FORCE=""  # don't overwrite the folder
HEADERS="1"  # check for Gutenberg headers
OUTPUTDIR="book-page-images"
PARALLEL=$(($(nproc)/2))
POINTSIZE=20  # how big a font to use
MAXPAGES=1000000000  # at most process 1G pages
THAICORPUS=""  # Do not do thai corpus-related conversions
VERBOSE=1  # print as you go
WRAP=""  # do not wrap lines

while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
      -f|--force)
	  FORCE=1
	  shift
	  ;;
      --font)
  	  FONT="$2"
  	  shift # past argument
  	  shift # past value
	  ;;
      -h|--help)
  	  echo "usage: $0 [-l n] <inputfile.txt>"
	  echo "split the inputfile into page images, add noise, convert back"
	  echo "the results will be put in the directory named '${OUTPUTDIR}'"
	  echo "accepts input file in .bz2 compressed format"
	  echo "  -f, --force     : force the output dir to be overwritten"
	  echo "  -font           : specify font for text processing"
	  echo "                    e.g. 'Noto-Sans-CJK-HK' for ZN"
	  echo "                         'liberation-serif' for RU"
	  echo "                         'Noto-Serif-Thai-Regular' for TH"
	  echo "  --no-headers     : do not check for Gutenberg headers"
  	  echo "  -l <n>, --lines   : put n lines per page (default 50)"
  	  echo "  -o, --output <dir>: put output into <dir>"
	  echo "  -p, --parallel   : image conversion parallelism"
	  echo "  --pointsize       : font size in points (default 12)"
	  echo "  --thai-corpus     : preprocess for Thai corpus texts cleaning"
	  echo "  -v <v>, --verbose: verbosity leve (default $VERBOSE)"
	  echo "  -w, --verbose    : specify line wrap column (default: no wrap)"
  	  exit 0
  	  ;;
      -l|--lines)
  	  SPLITLINES="$2"
  	  shift # past argument
  	  shift # past value
	  ;;
      -m|--max-pages)
  	  MAXPAGES="$2"
  	  shift # past argument
  	  shift # past value
	  ;;
      --no-headers)
	  HEADERS=""
	  shift # past argument
	  ;;
      -o|--output)
  	  OUTPUTDIR="$2"
  	  shift; shift
	  ;;
      -p|--PARALLEL)
	  PARALLEL=1
	  shift # past argument
	  ;;
      --pointsize)
	  POINTSIZE="$2"
	  shift
	  shift
	  ;;
      --thai-corpus)
	  THAICORPUS="thai"
	  shift
	  ;;
      -v|--verbose)
	  VERBOSE="$2"
	  shift; shift
	  ;;
      -w|--wrap)
	  WRAP="$2"
	  shift; shift
	  ;;
      *)    # unknown option
	  POSITIONAL+=("$1") # save it in an array for later
	  shift # past argument
	  ;;
  esac
done

if [ $VERBOSE -gt 0 ]; then
    echo "  pointsize (--pointsize)         :" ${POINTSIZE}
    echo "  page size in lines              :" ${SPLITLINES}
    echo "  max page count (-m, --max-pages):" $MAXPAGES
    echo "  Gutenberg headers (--no-headers):" ${HEADERS}
    echo "  outputdir (-o, --output)        :" ${OUTPUTDIR}
    echo "  parallelism (-p, --parallel)     :" ${PARALLEL}
    echo "  Thai corpus cleaning (--thai-corpus):" ${THAICORPUS}
    echo "  font (--font)                       :" ${FONT}
    echo "  verbose (-v, --verbose)             :" ${VERBOSE}
    echo "  wrap (-w, --wrap)                   :" ${WRAP}
fi
file=${POSITIONAL[0]}
if [ -z "$file" ]; then
    echo "please specify input file"
    exit 1
fi
if ! [ -f ${file} ]; then
    echo "file ${file} does not exist"
    exit 1
fi
dirname=$(dirname $(realpath $file))  # ensure we have absolute path name
file=$(basename $file)
basename=$([[ "$file" = *.* ]] && echo "${file%.*}" || echo "${file}")
nameroot=${file%%.*}  # remove all suffixes
if [ $VERBOSE -gt 0 ]; then
    echo "  input file: " $dirname/$file
    echo "splitting $file into chunks of $SPLITLINES lines"
fi
if [ -d $OUTPUTDIR ]; then
    if [ -z $FORCE ]; then
	echo "cannot proceed -- directory ${OUTPUTDIR} exists"
	echo "use --force to overwrite"
	exit 1
    fi
    if [ $VERBOSE -gt 0 ]; then
	echo "cleaning ${OUTPUTDIR}"
    fi
    rm -rf $OUTPUTDIR/*
else
    mkdir $OUTPUTDIR || exit 1
fi
cd $OUTPUTDIR

## copy/decompress the file
if [ $VERBOSE -gt 0 ]; then
    echo "get the text"
fi
## first decompress and send to a temporary file
tempfile=$(mktemp)
if [ "$(file -b -i $dirname/$file)" == "application/x-bzip2; charset=binary" ]; then
    # bz2 compressed
    if [ $VERBOSE -gt 0 ]; then
	echo "bz2 compressed file"
    fi
    bzip2 -dc $dirname/$file >${tempfile}
else
    if [ $VERBOSE -gt 0 ]; then
	echo "uncompressed text"
    fi
    cat $dirname/$file >$tempfile
fi

## Remove Gutenberg header/footer.
## These are always in EN and may mess with the language detection
## put the result in a new tmpfile
if ! [ -z $HEADERS ]; then
    if [ $VERBOSE -gt 0 ]; then
	echo "  remove Gutenberg headers"
    fi
    header="^\*\*\* *START OF"
    footer="^\*\*\* *END OF"
    if [ -z $(grep --files-with-matches "${header}" $tempfile) ]; then
	echo "Header missing in file!"
	header=""
    fi
    if [ -z $(grep --files-with-matches "${footer}" $tempfile) ]; then
	echo "Footer missing in file!"
	footer=""
    fi
    headerlessTempfile=$(mktemp)
    cat $tempfile |\
	grep -A 100000 --basic-regexp "${header}" |grep -B 100000 --basic-regexp "${footer}"|\
	tail +4 | head --lines=-4 >$headerlessTempfile
    rm $tempfile
    tempfile=$headerlessTempfile
fi

## Remove Thai corpus indicators.  These are
## * pipe symbols (word boundaries?)
## * words embedded into <tag>...</tag>
if ! [ -z $THAICORPUS ]; then
    if [ $VERBOSE -gt 0 ]; then
	echo "  Remove Thai corpus markers"
    fi
    thaiTempfile=$(mktemp)
    cat $tempfile |\
	sed -e "s/<[^>]*>[^<]\+<\/[^>]\+>//g"\
	    -e "s/|//g"\
	    -e "s/^\. //g"\
	    -e "s/\([ .]\):/\1/g"\
	    -e "s/ \.//g"\
	    >$thaiTempfile
    rm $tempfile
    tempfile=$thaiTempfile
    if [ $VERBOSE -gt 0 ]; then
	echo "   stored to ${tempfile}"
    fi
fi

## Wrap lines
if ! [ -z $WRAP ]; then
    if [ $VERBOSE -gt 0 ]; then
	echo "  Wrapping lines"
    fi
    wrappedTempfile=$(mktemp)
    cat $tempfile | sed "s/.\{"${WRAP}"\}/&\n/g" >$wrappedTempfile
    rm $tempfile
    tempfile=$wrappedTempfile
    if [ $VERBOSE -gt 0 ]; then
	echo "   done"
    fi
fi

## Split
if [ $VERBOSE -gt 0 ]; then
    echo "  Splitting lines"
fi
cat $tempfile |\
    split --lines=${SPLITLINES} --suffix-length=3 --additional-suffix=.txt - $nameroot- || exit 2
rm $tempfile
if [ $VERBOSE -gt 0 ]; then
    echo "   done"
fi

convert-to-image () {
    fName=$1
    # Extract just the name of the file without extention
    subbase=${fName%.txt}
    convert -size 3000x2000 xc:white -pointsize $POINTSIZE\
	    -fill black\
	    -font "${FONT}"\
	    -annotate +$POINTSIZE+$POINTSIZE "@$fName"\
	    -trim -bordercolor "#FFF" -border 20 +repage\
	    ${subbase}.jpg
    rm $fName
}
export -f convert-to-image
export POINTSIZE
export FONT

echo "Converting to jpg"
find . -name "${nameroot}-???.txt" | parallel -j $PARALLEL convert-to-image {}

if [ $VERBOSE -gt 0 ]; then
    echo "In total ${iPage} pages"
    echo "All done :-)"
fi
