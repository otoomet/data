# Geographic data


## Chilean boundary 

_chile-detailed-boundary_882.geojson_

Downloaded from https://cartographyvectors.com/map/882-chile-detailed-boundary

Outline of Chile, including fairly detailed fjords.



## countries-africa.csv

Downloaded from
[wikipedia](https://en.wikipedia.org/wiki/List_of_African_countries_by_population),
cleaned a bit with space etc in names.


## country-region.csv

Downloaded from https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv


## GSSHS

World vector maps.  Shorelines, political boundaries, lakes, rivers.

Downloaded from [NOAA](https://shoreline.noaa.gov/data/datasheets/wvs.html)


## states.csv: R dataset

Data about 50 US states, collected in 1970s:

* **Population**: population estimate as of July 1, 1975
* **Income**: per capita income (1974)
* **Illiteracy**: illiteracy (1970, percent of population)
* **Life Exp**: life expectancy in years (1969-71)
* **Murder**: murder and non-negligent manslaughter rate per
    100,000 population (1976)
* **HS Grad**: percent high-school graduates (1970)
* **Frost**: mean number of days with minimum temperature below
    freezing (1931-1960) in capital or large city
* **Area**: land area in square miles


## us-state-list-of-countries.csv

List of states from [US Dept of
State](https://www.state.gov/independent-states-in-the-world/) copied
2021-10-09.  Only one capital is preserved for each country, and notes
in parenthesis are removed.  Taiwan is included in the list.

Variables:

* **genc2a**, **genc3a** Geopolitical Entities, Names, and Codes
  (GENC) Standard two-letter and three-letter codes. GENC is the
  replacement standard for FIPS 10-4 and is the U.S. Government
  profile of the ISO 3166 international country code standard. For
  more information on GENC please see
  https://nsgreg.nga.mil/genc/discovery.



## Ukraine's regional population {#datasets-ukrainepopulation}

Copied from the
[Wikipedia table](https://en.wikipedia.org/wiki/List_of_Ukrainian_oblasts_and_territories_by_population)
2024-03-03.  Population as of 2015.

The variables are self-explanatory.


## Ukraine with regions {#datasets-ukrainemap}

The national borders and regional (_oblast_) borders of Ukraine in
_geojson_ format.  Provided by
[Cartography
Vectors](https://cartographyvectors.com/map/1530-ukraine-with-regions).

