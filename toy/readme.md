# Artificial toy datasets

## alcohol-disorders

A small subset of _health/alcohol-disorders.csv_.  It include
Argentina, Kenya, Taiwan, Ukraine, US for 2015-2019.  Intended for
demonstrating reshaping.


## dependent coin

Danish 2-kroner and 5-kroner taped together and flipped together, 50
times.  The sides are marked as _S_ for spiral and _M_ for Margrete's
initials.

## emperors

5 emperors with some basic data.  To demonstrate reading csv,
what happens if you get wrong separator, and what does `NA` mean. 


## flushot

Data about 8 patients: whether they got flushot (0/1) and whether they
got flu (0/1)


## PCA

2-D dataset with two columns: _x_, _y_.  _x_ and _y_ are strongly
correlated.  The dataset is intended to demonstrate PCA.


## schooling.csv

Fake data for DiD analysis.  The story is like this:

> Some provinces introduce new measures to
> boost schooling.  Data is collected before and 5 years after the reforms.

* **T** treatment group (province that introduced reform)
* **A** after treatment (5 years after the reform was introduced) 
* **S** average years of schooling in province.

## spiral.R: 

Create somewhat noisy spiral data with mix of circular/normal dots.
Run with `-h` option to see
help.


## treatment-ba.csv

demo data for BA estimator: outcome (y), time (1..6).  Treated after
time 3.


## treatment-cs.csv

treatment (T) and outcome (y), to demonstrate CS estimator


## treatment-did.csv

treatment (T), time, and outcome (y), to demonstrate diff-in-diff
estimator. 

## yin-yang.R

Create dots in a noisy Yin-Yang pattern.  Run with `-h` option to see
help.
