#!/usr/bin/env Rscript

## Create a multicolor spiral data with positive/negative values
library(magrittr)
library(data.table)
library(ggplot2)
source("../bin/utils.R")

args <- argparser::arg_parser("Create data spiral") %>%
   argparser::add_argument("--eps",
                           "normal noise on color", 0.4) %>%
   argparser::add_argument("--N",
                           "number of points", 100L) %>%
   argparser::add_argument("--outFName",
                           "output results into the file", "spiral.csv") %>%
   argparser::add_argument("--stretch",
                           "stretch perpendicular to stretch axis", 1) %>%
   argparser::add_argument("--stretchAngle",
                           "angle of the stretch axis (deg)", 45) %>%
   argparser::parse_args()
N <- args$N
cat(N, "datapoints\n")
outFName <- args$outFName
cat("output to", outFName, "\n")
eps <- args$eps
cat("normal noise on color", eps, "\n")
stretch <- args$stretch
cat("stretch factor", stretch, "\n")
stretchAngle <- args$stretchAngle
cat("normal noise on color", stretchAngle, "\n")

## Mixture of normal and circle
pN <- 0.8  # proportion of normal
NN <- N*pN
NC <- N - NN

xN <- rnorm(NN)
yN <- rnorm(NN)

R <- sqrt(runif(NC))
phi <- 2*pi*runif(NC)
xC <- R*cos(phi)
yC <- R*sin(phi)

x <- c(xN, xC)
y <- c(yN, yC)

## colors
phi <- atan2(y, x)
r <- sqrt(x*x + y*y)
c <- sin(3*phi + 4*r) + eps*rnorm(N)
color <- cut(c, c(-Inf, -0.7, 0, 0.7, Inf), labels=1:4)

## Stretch
X <- matrix(c(x, y), ncol=2)
X <- X %*% Rot(stretchAngle) %*% StretchX(stretch) %*% Rot(-stretchAngle)

dt <- data.table(x=X[,1], y=X[,2], c, color, sign=c>0)

## put in random order
dt <- dt[sample(N)]

##
cat("final data", nrow(dt), "obs\n")
writeTable(dt, outFName)
cat("all done\n")
