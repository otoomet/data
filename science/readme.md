# Science data

## Hubble

Data from the Hubble (1929) paper.  Original explanations:

* **ms** = photographic magnitude of brightest stars involved
* **r** = distance in units of 10^6 parsecs.  The first two are
    Shapley's values.
* **v** = measured velocities inkm./sec.  N.G.C. 6822, 221, 224 and
  5457 are recent determinations by Humanson. 
* **mt** = Hoetschek's visual magnitude as corrected by Hopmann.  The
  first three objects were not measured by Holetschek, and the values
  of mt represent estimates by the author based upon such data as are
  available. 
* **Mt** = total visual absolute magnitude computed from mt and r.
* **D**: estimated distance from mt, Mt (not in the original data)

Additional variables

* **RModern**: modern distance estimate (Mpc), mostly from wikipedia
* **vModern**: modern radial velocity estimates (km/s), mostly from
  wikipedia 
