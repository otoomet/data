# Data about games, in particular video games

## CS-GO reviews full

**csgo-reviews-full.csv.bz2**: downloaded from
https://github.com/mulhod/steam_reviews/blob/master/data/Counter_Strike_Global_Offensive.jsonlines,
and converted to csv using `stream_in(..., flatten = TRUE)`.

Columns:

* **num_workshop_items"**
* **profile_url"**
* **rating"**
* **num_voted_helpfulness"**
* **num_found_helpful"**
* **friend_player_level"**
* **username"**
* **num_screenshots"**
* **num_found_unhelpful"**
* **orig_url"**
* **steam_id_number"**
* **date_posted"**
* **total_game_hours_last_two_weeks"**
* **num_games_owned"**
* **date_updated"**
* **num_groups"**
* **num_found_funny"**
* **num_comments"**
* **num_badges"**
* **total_game_hours"**
* **review"**
* **found_helpful_percentage"**
* **num_reviews"**
* **review_url"**
* **num_guides"**
* **num_friends"**
* **achievement_progress.num_achievements_possible"**
* **achievement_progress.num_achievements_percentage"**
* **achievement_progress.num_achievements_attained"**

Original states MIT license.



## CS-GO reviews subset

**csgo-reviews.csv.bz2**: subset of _csgo-reviews-full.csv.bz2_,
column names simplified:

Columns:

* **rating"**
* **nHelpful"** number voted helpful
* **nFunny"** number found funny
* **nScreenshots"** number of screenshots
* **date"** date posted
* **hours"** total game hours
* **nGames"** number of games
* **nReviews"** number of reviews
