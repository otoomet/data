# Data for Teaching

This repo is mainly to store 
code and documentation
for downloading, preparing and using data in teaching/problemsets.
The
datasets itself are (in most cases) not included.

* _health_: health, illness etc.  COVID is a separate folder.
* _nature_: planet Earth, biology, etc.  See also _science_
* _science_: also nature, but more like not planet Earth and not
  (earthly) biology
* _social_: social sciences, human data, crime, other
  macro social data but 
  not related to health
* _toy_: small toy datasets
* _images_: image data, contains enormous subfolders


## covid

dowload updated covid data
[https://github.com/datasets/covid-19/blob/master/data/time-series-19-covid-combined.csv](github.com/datasets/covid-19).
Merge it with country codes data (from iban.org) and with national
lockdown dates (from Business Insider, many missing).  The concept of national
lockdown is fuzzy though, in many places the lockdown was rolled out
regionwise, and in increasingly stricts steps.

Prepare data for assignments.


## social

Various social-science related data, including names, drinking,
budget, crime, WVS.
