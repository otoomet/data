# Sports data

## dawgs-dash-5k-2023.csv

Alaska Airlines _Dawgs Dash_, Oct 08, 2023.  Downloaded from the web.
Two files are included, the _-names_ includes names.

* **rank**: place in the race.  It is min rank, so if several runners
  share the same time, all they get the lowest rank there, but the
  next ones will be ranked lower by the number of shared-time runners.
* **name**: not to be shared
* **category**: in the form **age | gender** (gender is M/F)
* **time**: either `mm:ss` or `hh:mm:ss`, depending on if the time
  was less or more than 1h.
* **bib**: bib number of the runner

## fox-deaaron.csv: DeAaron Fox 2023-2024 season results

DeAaron Fox is an NBA palyer.

Data from 
[baseballreference.com](https://www.basketball-reference.com/players/f/foxde01/gamelog/2024)
See _harden.csv_ for details.


## harden.csv: James Harden 2018-2019 results

Harden is an NBA player, 196 cm, 99 kg, born 1989.

Data from
https://www.basketball-reference.com/players/h/hardeja01/gamelog/2019/
(for 2018-2019) and from 
https://www.basketball-reference.com/players/h/hardeja01/gamelog/2022
(for the 2021-2022 season).

* **Rk**: rank (game id)
* **G**: season game for Harden
* **Date**: yyyy-mm-dd
* **Age**: years-days
* **Tm**: team
* **Opp**: opponent
* **GS**: games started (=1 as each row is one game)
* **MP**: minutes played
* **FG**: field goals
* **FGA**: field goal attempts
* **FG%**: field goal percentage scored
* **3P**: 3-point field goals 
* **3PA**: 3-point attempts
* **3P%**: 
* **FT**: free throw scores
* **FTA**: free throw attempts
* **FT%**:
* **ORB**: offensive rebounds
* **DRB**: defensive rebounds
* **TRB**: total rebounds
* **AST**: assists
* **STL**: steals
* **BLK**: blocks
* **TOV**: turnovers
* **PF**: personal fouls
* **PTS**: points
* **GmSc**: game score
* **+/-**: 


## Marathon times

Downloaded from  https://raw.githubusercontent.com/jakevdp/marathon-data/master/marathon-data.csv
