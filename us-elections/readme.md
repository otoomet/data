# Datasets, related to US elections

## County population and demographics: _co-estYYYY-alldata.csv.bz2_

Downloaded from US Census:
https://www2.census.gov/programs-surveys/popest/datasets/2020-2021/counties/totals/
There is also a brief explanation in _co-est21-alldata.pdf_.  It is
for a newer dataset, but seems similar.

_co-est2023-alldata.csv_: downloaded from 
https://www.census.gov/data/datasets/time-series/demo/popest/2020s-counties-total.html
included pdf codebook


## Codebook for 2000-2020 County Presidential Data

The data file `countypres_2000-2016` contains county-level returns for
presidential elections from 2000 to 2016. The data source is official
state election data records.

Note: County results in Alaska for 2004 are based on official Alaska
data, but it is clear the district returns significantly overstate the
number of votes cast.

2020 elections data from [github](https://raw.githubusercontent.com/tonmcg/US_County_Level_Election_Results_08-20/master/2020_US_County_Level_Presidential_Results.csv)

County background data from BEA and US Census

### Variables
The variables are listed as they appear in the data file.  The complex
names originate from the census tables.

- **FIPS**: county FIPS code
- **year**: election year	
- **state**: state name 
- **state2**: 2-letter state code
- **region**: census region (west, midwest, south, northeast)
- **county**: county name
- **office**: President (we look only at presidential elections)
- **candidate**: name of the candidate
- **party**: party of the candidate
- **candidatevotes**: votes received by this candidate for this particular party
- **totalvotes**: total number of votes cast in this county-year
- **income**: personal income, USD/per capita (BEA data)
- **population**: population, census estimate (BEA data)
- **LND010200D**: land area (sq.mi) at 2000 (Census data)
- **EDU600209D**: Persons 25 years and over, total 2005-2009
- **EDU695209D**: Educational attainment - persons 25 years and over - bachelor's degree 2005-2009
- **POP010210D**: Resident population (April 1 - complete count) 2010
- **POP220210D**: Population of one race - White alone 2010 (complete count)
- **POP250210D**: Population of one race - Black or African American alone 2010 (complete count)
- **POP320210D**: Population of one race - Asian alone 2010 (complete count)
- **POP400210D**: Hispanic or Latino population 2010 (complete count)
- **PST110209D**: Resident total population estimate, net change - April 1, 2000 to July 1, 2009
- **BIRTHS2020**: Births in period 4/1/2020 to 6/30/2020
- **DEATHS2020**: Deaths in period 4/1/2020 to 6/30/2020

## Files

* -wide: the same data in wide form using fewer variables


## us-elections_2000-2024.csv

Downloaded from
[tonmcg on GH](https://github.com/tonmcg/US_County_Level_Election_Results_08-24)


## us-presidential-elections_2000-2016.csv

The data file `countypres_2000-2016` contains county-level returns for
presidential elections from 2000 to 2016. The data source is official
state election data records.

Note: County results in Alaska for 2004 are based on official Alaska
data, but it is clear the district returns significantly overstate the
number of votes cast.

- **year**: election year	
- **state**: state name 
- **state_po**: U.S. postal code state abbreviation
- **county**: county name
- **FIPS**: county FIPS code
- **office**: President
- **candidate**: name of the candidate
- **party**: party of the candidate
- **candidatevotes**: votes received by this candidate for this particular party
- **totalvotes**: total number of votes cast in this county-year
- **version**: date when dataset was finalized

## CAINC1-2024.zip

Income by counties.
US Bureau of Economic Analysis data, from 
https://apps.bea.gov/regional/downloadzip.htm
