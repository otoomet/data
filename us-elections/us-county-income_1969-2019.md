# US Income by county

Downloaded from Bureau of Economic Analysis.

## Definitions from included XML file:

* _Personal income_ (thousands of dollars): Consists of the income that
  persons receive in return for their provision of labor, land, and
  capital used in current production as well as other income, such as
  personal current transfer receipts. In the state and local personal
  income accounts the personal income of an area represents the income
  received by or on behalf of the persons residing in that area. It is
  calculated as the sum of wages and salaries, supplements to wages
  and salaries, proprietors' income with inventory valuation (IVA) and
  capital consumption adjustments (CCAdj), rental income of persons
  with capital consumption adjustment (CCAdj), personal dividend
  income, personal interest income, and personal current transfer
  receipts, less contributions for government social insurance plus
  the adjustment for
  residence.
* _Population_ (persons): The number of individuals (both
  civilian and military) who reside in a given
  area.
* _Per capita
  personal income_ (dollars) The personal
  income of a given area divided by the resident population of the
  area. See _personal income_.


## Footnotes at the end of tables:

* Note: See the included footnote file.
* CAINC1: Personal Income Summary: Personal Income, Population, Per Capita Personal Income
* Last updated: November 17, 2020-- new statistics for 2019; revised statistics for 2010-2018.
* Source: U.S. Department of Commerce / Bureau of Economic Analysis / Regional Economic Accounts

## CAINC1 Footnotes from attache HTML doc:

  * 1/ Census Bureau midyear population estimates. Estimates for 2010-2019 reflect county population estimates available as of
    March 2020.
  * 2/ Per capita personal income was computed using Census Bureau midyear population estimates. Estimates for 2010-2019 reflect
    county population estimates available as of March 2020.
  * Cibola, NM was separated from Valencia in June 1981, but in these estimates, Valencia includes Cibola through the end of
    1981.
  * La Paz County, AZ was separated from Yuma County on January 1, 1983. The Yuma, AZ MSA contains the area that became La Paz
    County, AZ through 1982 and excludes it beginning with 1983.
  * Broomfield County, CO, was created from parts of Adams, Boulder, Jefferson, and Weld counties effective November 15, 2001.
    Estimates for Broomfield county begin with 2002.
  * Shawano, WI and Menominee, WI are combined as Shawano (incl. Menominee), WI for the years prior to 1989.
  * Estimates for 1979 forward reflect Alaska Census Areas as defined by the Census Bureau; those for prior years reflect Alaska
    Census Divisions as defined in the 1970 Decennial Census. Estimates from 1988 forward separate Aleutian Islands Census Area
    into Aleutians East Borough and Aleutians West Census Area. Estimates for 1991 forward separate Denali Borough from
    Yukon-Koyukuk Census Area and Lake and Peninsula Borough from Dillingham Census Area. Estimates from 1993 forward separate
    Skagway-Yakutat-Angoon Census Area into Skagway-Hoonah-Angoon Census Area and Yakutat Borough. Estimates from 2008 forward
    separate Skagway-Hoonah-Angoon Census Area into Skagway Municipality and Hoonah-Angoon Census Area. Estimates from 2009 forward
    separate Wrangell-Petersburg Census Area into Petersburg Census Area and Wrangell City and Borough. In addition, a part of the
    Prince of Wales-Outer Ketchikan Census Area was annexed by Ketchikan Gateway Borough and part (Meyers Chuck Area) was included
    in the new Wrangell City and Borough. The remainder of the Prince of Wales-Outer Ketchikan Census Area was renamed Prince of
    Wales-Hyder Census Area. Petersburg Borough was created from part of former Petersburg Census Area and part of Hoonah-Angoon
    Census Area for 2013 forward. Prince of Wales-Hyder Census Area added part of the former Petersburg Census Area beginning in
    2013. For years 2009-2012, Petersburg Borough reflects the geographic boundaries of the former Petersburg Census Area. Wade
    Hampton Census Area was renamed Kusilvak Census Area on July 1, 2015.
  * Virginia combination areas consist of one or two independent cities with 1980 populations of less than 100,000 combined with
    an adjacent county. The county name appears first, followed by the city name(s). Separate estimates for the jurisdictions
    making up the combination area are not available. Bedford County, VA includes the independent city of Bedford for all years.
  * Shannon County, SD was renamed to Oglala Lakota County, SD on May 1, 2015.
  * Kalawao County, Hawaii is combined with Maui County. Separate estimates for the jurisdictions making up the combination areas
    are not available.
  * Nonmetropolitan portion includes micropolitan counties.
  * Metropolitan Areas are defined (geographically delineated) by the Office of Management and Budget (OMB) bulletin no. 20-01
    issued March 6, 2020.
  * Note-- All dollar estimates are in thousands of current dollars (not adjusted for inflation). Calculations are performed on
    unrounded data.
  * (NA) Not available.
  * (NM) Not meaningful.
  * Last updated: November 17, 2020-- new statistics for 2019; revised statistics for 2010-2018.

Source: Department of Commerce / Bureau of Economic Analysis / Regional Economic Accounts
