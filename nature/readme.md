# Nature data

## HADCRUT 5.0.1.0

### Annual

HadCRUT (Hadley Centre/Climatic Research Unit Temperature)
dataset is maintained by Met Office (UK Government).
The data source includes both annual and montly time series.
Copyrighted under
[Open Government
License](https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/). 
Downloaded from
[Metoffice download page](https://www.metoffice.gov.uk/hadobs/hadcrut5/data/HadCRUT.5.0.2.0/download.html)
as
[Summary/global/annual](https://www.metoffice.gov.uk/hadobs/hadcrut5/data/HadCRUT.5.0.2.0/analysis/diagnostics/HadCRUT.5.0.2.0.analysis.summary_series.global.annual.csv)

The variables are:

* **Time** year
* **Anomaly (deg C)**: relative to 1961-1990 average
* **Lower confidence limit (2.5%)**
* **Upper confidence limit (97.5%)**

### Monthly

Downloaded from 
[Metoffice download page](https://crudata.uea.ac.uk/cru/data/temperature/HadCRUT5.0Analysis_gl.txt)

* **year**
* **1-12** months


## java-earthquakes

Earthquakes 2000-2020 in Java Sea region, stronger than mag 5.  Data
from
[USGS](https://earthquake.usgs.gov/earthquakes/search/#%7B%22currentfeatureid%22%3Anull%2C%22mapposition%22%3A%5B%5B-46.58907%2C-265.56152%5D%2C%5B43.06889%2C-230.58105%5D%5D%2C%22autoUpdate%22%3A%5B%22autoUpdate%22%5D%2C%22feed%22%3A%22search_undefined%22%2C%22listFormat%22%3A%22default%22%2C%22restrictListToMap%22%3A%5B%5D%2C%22sort%22%3A%22newest%22%2C%22basemap%22%3A%22grayscale%22%2C%22overlays%22%3A%5B%22plates%22%5D%2C%22timezone%22%3A%22utc%22%2C%22viewModes%22%3A%5B%22list%22%2C%22settings%22%2C%22map%22%5D%2C%22event%22%3Anull%2C%22search%22%3A%7B%22name%22%3A%22Search%20Results%22%2C%22params%22%3A%7B%22starttime%22%3A%222000-02-15%2000%3A00%3A00%22%2C%22endtime%22%3A%222021-02-22%2023%3A59%3A59%22%2C%22maxlatitude%22%3A7.378%2C%22minlatitude%22%3A-12.281%2C%22maxlongitude%22%3A-230.098%2C%22minlongitude%22%3A-266.045%2C%22minmagnitude%22%3A6%2C%22orderby%22%3A%22time%22%7D%7D%7D)

Variables (see
[USGS](https://earthquake.usgs.gov/data/comcat/data-eventterms.php#time)
for more detailed description):

- **time**: in UTC
- **latitude**: decimal degrees, north positive
- **longitude**: decimal degrees, east positive
- **depth**: depth of event, km
- **mag**: magnitude
- **magType**: method use to compute _mag_
- **nst**: number of stations used to determine the location
- **gap**: The largest azimuthal gap between azimuthally adjacent stations (in degrees).
- **dmin**: Horizontal distance from the epicenter to the nearest station (in degrees)
- **rms**: The root-mean-square (RMS) travel time residual, in sec, using all weights
- **net**: The ID of a data contributor.
- **id**: A unique identifier for the event. 
- **updated**: Time when the event was most recently updated. 
- **place**: Textual description of named geographic region near to the event.
- **type**: Type of seismic event.
- **horizontalError**: Uncertainty of reported location of the event in kilometers.
- **depthError**: Uncertainty of reported depth of the event in kilometers.
- **magError**: Uncertainty of reported magnitude of the event. The
  estimated standard error of the magnitude. 
- **magNst**: The total number of seismic stations used to
  calculate the magnitude for this earthquake.
- **status**: Indicates whether the event has been reviewed by a human.
- **locationSource**: The network that originally authored the reported location of this event. 
- **magSource**: Network that originally authored the reported magnitude for this event. 

## ice-extent

Sea ice extent and are in northern hemisphere.  NSIDC info page:

> Sea Ice Index, Version 3
> 
> The Sea Ice Index provides a quick look at Arctic- and
> Antarctic-wide changes 
> in sea ice. It is a source for consistent, up-to-date sea
> ice extent and concentration images, in PNG format, and data
> values, 
> in GeoTIFF and ASCII text files, from November 1978 to the
> present. Sea
> Ice Index images also depict trends and anomalies in ice cover
> calculated using a 30-year reference period of 1981 through 2010.
> 
> The images and data are produced in a consistent way that makes
> the Index time-series appropriate for use when looking at
> long-term
> trends in sea ice cover. Both monthly and daily products are
> available. However, monthly products are better to use for
> long-term trend analysis because errors in the daily product
> tend to be averaged out in the monthly product and because
> day-to-day variations are often the result of short-term weather.

Downloaded from [U Colorado](ftp://sidads.colorado.edu/DATASETS/NOAA/G02135/north/monthly/data/)

I haven't found description of the variables, but these are fairly
self-explanatory: 

- **year**
- **month** (1-12)
- **data-type**: looks like the name of the satellite or another info
  provider 
- **region**: "N" for northern, "S" for southern hemisphere
- **extent**: sea ice extent, in M km2.  Extent is the sea surface are
  where the ice concentration is at least 15%.
- **area**: sea ice surface area, M km2
- **time**: a continuous time variable, made of year and month


## NOAA global temperature

Downloaded from
https://www.ncei.noaa.gov/access/monitoring/global-temperature-anomalies/anomalies
(just click the "download" button in there).

Global Land and Ocean Temperature Anomalies
Units: Degrees Celsius
Base Period: 1901-2000
Missing: -999

Variables:

* **Year**: in form yyyymm
* **Anomaly**: global temperature anomaly wrt 1901-2000 base period,
  deg C.


## NYC Central Park squirrel census

Downloaded from [NYC Open
Data](https://data.cityofnewyork.us/Environment/2018-Central-Park-Squirrel-Census-Squirrel-Data/vfnx-vebw).
There is also additional documentation.

Usage is covered by [NY Open Data Terms of Usage](https://opendata.cityofnewyork.us/overview/#termsofuse).
[FAQ](https://opendata.cityofnewyork.us/faq/) states that

> Open Data belongs to all New Yorkers. There are no restrictions on
> the use of Open Data. Refer to Terms of Use for more information. 

Variables:

* **X**: Longitude coordinate for squirrel sighting point
* **Y**: Latitude coordinate for squirrel sighting point
* **Unique Squirrel ID**: Identification tag for each squirrel
  sightings. The tag is comprised of "Hectare ID" + "Shift" + "Date" +
  "Hectare Squirrel Number."
* **Hectare**: ID tag, which is derived from the hectare grid used to
  divide and count the park area. One axis that runs predominantly
  north-to-south is numerical (1-42), and the axis that runs
  predominantly east-to-west is roman characters (A-I).
* **Shift**: Value is either "AM" or "PM," to communicate whether or
  not the sighting session occurred in the morning or late afternoon.
* **Date**: Concatenation of the sighting session day and month.
* **Hectare Squirrel Number**: Number within the chronological
  sequence of squirrel sightings for a discrete sighting session.
* **Age**: Value is either "Adult" or "Juvenile."
* **Primary Fur Color**: Value is either "Gray," "Cinnamon" or
  "Black."
* **Highlight Fur Color**: Discrete value or string values comprised
  of "Gray," "Cinnamon" or "Black."
* **Combination of Primary and Highlight Color**: A combination of the
  previous two columns; this column gives the total permutations of
  primary and highlight colors observed.
* **Color notes**: Sighters occasionally added commentary on the
  squirrel fur conditions. These notes are provided here.
* **Location**: Value is either "Ground Plane" or "Above Ground."
  Sighters were instructed to indicate the location of where the
  squirrel was when first sighted.
* **Above Ground Sighter Measurement**: For squirrel sightings on the
  ground plane, fields were populated with a value of “FALSE.”
* **Specific Location**: Sighters occasionally added commentary on the
  squirrel location. These notes are provided here.
* **Running**: Squirrel was seen running.
* **Chasing**: Squirrel was seen chasing another squirrel.
* **Climbing**: Squirrel was seen climbing a tree or other
  environmental landmark.
* **Eating**: Squirrel was seen eating.
* **Foraging**: Squirrel was seen foraging for food.
* **Other Activities**:
* **Kuks**: Squirrel was heard kukking, a chirpy vocal communication
  used for a variety of reasons.
* **Quaas**: Squirrel was heard quaaing, an elongated vocal
  communication which can indicate the presence of a ground predator
  such as a dog.
* **Moans**: Squirrel was heard moaning, a high-pitched vocal
  communication which can indicate the presence of an air predator
  such as a hawk.
* **Tail flags**: Squirrel was seen flagging its tail. Flagging is a
  whipping motion used to exaggerate squirrel's size and confuse
  rivals or predators. Looks as if the squirrel is scribbling with
  tail into the air.
* **Tail twitches**: Squirrel was seen twitching its tail. Looks like
  a wave running through the tail, like a breakdancer doing the arm
  wave. Often used to communicate interest, curiosity.
* **Approaches**: Squirrel was seen approaching human, seeking food.
* **Indifferent**: Squirrel was indifferent to human presence.
* **Runs from**: Squirrel was seen running from humans, seeing them as
  a threat.
* **Other Interactions**: Sighter notes on other types of interactions
  between squirrels and humans.
* **Lat/Long**: Latitude and longitude


## Scripps CO2 data

This is the main Keeling Curve data from
[Scripps CO2 Program](https://scrippsco2.ucsd.edu/data/atmospheric_co2/primary_mlo_co2_record.html).
The monthly dataset is downloadable at
https://scrippsco2.ucsd.edu/assets/data/atmospheric/stations/in_situ_co2/monthly/monthly_in_situ_co2_mlo.csv
It is basically a csv file with extensive header:

> The data file below contains 10 columns.  Columns 1-4 give the dates in several redundant
> formats. Column 5 below gives monthly Mauna Loa CO2 concentrations in micro-mol CO2 per
> mole (ppm), reported on the 2012 SIO manometric mole fraction scale.  This is the
> standard version of the data most often sought.  The monthly values have been adjusted
> to 24:00 hours on the 15th of each month.  Column 6 gives the same data after a seasonal
> adjustment to remove the quasi-regular seasonal cycle.  The adjustment involves
> subtracting from the data a 4-harmonic fit with a linear gain factor.  Column 7 is a
> smoothed version of the data generated from a stiff cubic spline function plus 4-harmonic
> functions with linear gain.  Column 8 is the same smoothed version with the seasonal
> cycle removed.  Column 9 is identical to Column 5 except that the missing values from
> Column 5 have been filled with values from Column 7.  Column 10 is identical to Column 6
> except missing values have been filled with values from Column 8.  Missing values are
> denoted by -99.99
> Column 11 is the 3-digit sampling station identifier"
>
> CO2 concentrations are measured on the '12' calibration scale

Data is licensed by
[CC Atribution 4.0 license](http://creativecommons.org/licenses/by/4.0/)

Variables:

* **year**
* **month**
* **date (excel)**
* **date (year)**
* **co2**
* **co2 (seasonally adjusted)**
* **co2 (seasonal, smoothed)**
* **co2 (smoothed)**
* **co2 (filled)**
* **co2 (adjusted, filled)**
* **station**



## UAH lower troposhphere temperature

Downloaded from
[UAH](https://www.nsstc.uah.edu/data/msu/v6.0/tlt/uahncdc_lt_6.0.txt),
renamed variables, include the long form.

For wide form, the variables are 

* **year** 1978-
* **month** 1-12
* **globe** global "lower troposhpere" temperature anomaly, compared
  to 1991-2020 base period, in deg C.
* **globe_land** global land temperature anomaly
* **globe_ocean**
* **nh** north hemisphere
* **nh_land**
* **nh_ocean**
* **sh** south hemisphere
* **sh_land**
* **sh_ocean**
* **trpcs** tropics (-30 - 30deg)
* **trpcs_land**
* **trpcs_ocean**
* **noext** northern "extent" (20 - 90deg)
* **noext_land**
* **noext_ocean**
* **soext** southern extent
* **soext_land**
* **soext_ocean**
* **nopol** northern polar (60-90deg)
* **nopol_land**
* **nopol_ocean**
* **sopol** southern polar
* **sopol_land**
* **sopol_ocean**
* **usa48** lower 48
* **usa49** lower 48 + Alaska
* **aust** Australia


## Lunar Impact Craters

Downloaded from [Lunar and Planetary
Institute](https://www.lpi.usra.edu/scientific-databases/).  Includes
and excel file with more descriptions.

Variables:

1. Crater name. Basin names (Wilhelms 1987) underlined, Apollo landing site crater names (Müller and Jappel 1977) in italics, officially approved (IAU-USGS) craters in normal font. 
2. Diameter [km]. Most of the small Apollo landing site crater diameters are taken from Müller & Jappel (1977).
3. Latitude (positive north, negative south)
4. Longitude (positive east, negative west)
5. East longitude (0-360 degrees)
6. Radius [km]
7. Radius [m]
8. Apparent diameter [km], measured at the pre-impact surface level, according to Pike (1977b).
9. Transient cavity diameter [km], for simple craters (smaller than 15 km) equation Dtc=0.84D from Melosh (1989, p. 129) is applied, for complex craters larger than 15 km equation 9 from Croft (1985) is applied.
10. Transient cavity diameter [km] for complex craters. Equation 5 from Kring (1995) is applied with the assumption that transient cavity radius of complex and simple craters are the same. (0 means that value can not be calculated for simple craters, i.e. smaller than 15 km in diameter.)
11. Floor diameter [km], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Pike 1977).  (0 means that basin is too big to be able to use the equation accurately for craters smaller than 20 and larger than 125 km in diameter.)
12. Measured rim to floor depth, mainly from Arthur (1974) with additional data from Pike (1976). When both values were available, Arthur's data was used.
13. Rim to floor depth [km], calculated based on Pike 1977a. (0 means that basin is too big to be able to use the equation accurately for craters smaller than 15 km and larger than 275 km in diameter.)
14. Apparent depth [km], measured at the pre-impact surface level, according to Pike (1977b).
15. Transient cavity depth [km] equal to 1/3 of transient cavity diameter Stöffler et al. (2006).
16. Interior volume [km^3], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Croft 1978). (0 means that value can not be calculated accurately for craters smaller than 13 and larger than 150 km in diameter.) 
17. Rim height [km], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Pike 1977a).  (0 means that basin is too big to be able to use the equation accurately for craters smaller than 15 km and larger than 375 km in diameter.)
18. Rim flank width [km], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Pike 1977).  (0 means that basin is too big to be able to use the equation efficiently.)
19. Measured central peak height, from Wood 1973 (The actual central peak heights are available at: http://www.lpod.org/cwm/DataStuff/CP-heights.html).
20. Height of central peak [km], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Hale and Grieve (1982)).  (0 means that value cannot be calculated accurately for craters smaller than 17 km and larger than 51 km in diameter.) 
21. Diameter of central peak [km], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Hale and Head (1979)).  (0 means that value cannot be calculated accurately for craters smaller than 17 km and larger than 175 km in diameter.)
22. Basal area of central peak [km^2], calculated based on Hörz et al. (1991, table 4.1, pp. 66; Hale and Grieve (1982)).  (0 means that value cannot be calculated accurately for craters smaller than 17 km and larger than 136 km in diameter.) 
23. Maximum diameter of ejecta blocks [km] - coefficient 2, calculated from Hörz et al. (1991, table 4.3, pp. 72; based on Moore (1971)).  (0 means that value cannot be calculated accurately for craters smaller than 0.1 and larger than 98 km in diameter.) 
24. Maximum diameter of ejecta blocks [km] - coefficient 1, calculated from Hörz et al. (1991, table 4.3, pp. 72; based on Moore (1971)).  (0 means that value cannot be calculated accurately for craters smaller than 0.1 and larger than 98 km in diameter.) 
25. Thickness of ejecta in the distance equal to one radius [m] - ejecta on the rim, for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) suggest that values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equation to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
26. Thickness of ejecta in the distance equal to two radii [m], for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) suggest that values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equation to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
27. Thickness of ejecta in the distance equal to three radii [m], for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) suggest that values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equation to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
28. Thickness of ejecta in the distance equal to four radii [m], for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) suggest that values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equation to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
29. Thickness of ejecta in the distance equal to five radii [m], for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) suggest that values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equation to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
29. Thickness of ejecta in the distance of 10,000 m outside the rim [m], for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) suggest that values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equation to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center).
31. Thickness of ejecta in the distance of 10,000 m outside the rim [m], calculated based on equation no 9 from Pike (1974), values for large multiring basins can be ambiguous. 
32. Thickness of ejecta in the distance of 10,000 m outside the rim [m], calculated based on equation no 10 from Pike (1974), values for large multiring basins can be ambiguous. 
33. Thickness of ejecta in the distance of 10,000 m outside the rim [m], calculated based on equation no 12 from Pike (1974), values for large multiring basins can be ambiguous. 
34. Radial distance of continuous ejecta [km] from the center of the crater, calculated based on Moore et al. (1974).  (0 means that value cannot be calculated accurately for craters smaller than 0.65 km or larger than 218 km in diameter.) 
35. Radial distance of continuous ejecta [km], calculated after Hörz et al. (1991, table 4.3, pp. 72, based on Fig. 8 in Oberbeck et al. (1974)).  (0 means that value cannot be calculated accurately for craters smaller than 0.56 km or larger than 1340 km in diameter.) 
36. Radius of ejecta blanket thicker than 10 m [km], equation no 9 from Pike (1974), values for large multiring basins can be ambiguous. 
37. Radius of ejecta of thicker than 10 m [km] (minimum estimate), for simple craters equation 4 from Kring (1995), for complex craters (D>15000 m) calculated based on equation from McGetchin et al. (1973). Minimum and maximum exponents are from Kring (1995), based on McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equations to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
38. Radius of ejecta of thicker than 10 m [km] (best estimate),  for simple craters equation 4 from Kring (1995), for complex craters (D>15) calculated based on equation from McGetchin et al. (1973). Minimum and maximum exponents are from Kring (1995), based on McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equations to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
39. Radius of ejecta of thicker than 10 m [km] (maximum estimate), for simple craters equation 4 from Kring (1995), for complex craters (D>15) calculated based on equation from McGetchin et al. (1973). Minimum and maximum exponents are from Kring (1995), based on McGetchin et al. (1973). Equation used for complex craters was used in the original paper to calculate ejecta thickness of large basins like Imbrium in the Apollo landing sites, other authors (Hörz et al. 1991) values for large multiring basins can be ambiguous. Pike (1974) suggested 3 other equations to calculate thickness of ejecta blankets on the Moon that vary significantly (also calculated in this database). Because the equations are not calibrated in the reliable way, it is very hard to evaluate their precision. Usually the distance of continuous ejecta is assumed to be equal to distance of 2 or 3 radii (from crater center). 
40. Radius (from the center of the crater) of radar-bright halos (at 70 cm wavelength) emplaced ballistically around craters [km], from Eq. 8 in Ghent et al. (2010). Equation based on craters approximately D=5...130 km.
41. Measured radius (from the center of the crater) of radar-bright halos (at 70 cm wavelength) emplaced ballistically around craters [km], from Table 1 in Ghent et al. (2010).
42. Radius of radar-dark halos (at 70 cm wavelength) around craters [km], measured from the center of the crater. Based on data from Table 1 in Ghent et al. (2010), excluding Sinus Iridum and Orientale (too few data points in such large diameters). Thus, the craters range D=5.6…191 km. The equation is derived by plotting the data and fitting a power function in Excel. 
43. Measured radius of radar-dark halos (at 70 cm wavelength) around craters [km], measured from the center of the crater. Taken from Table 1 in Ghent et al. (2010).
44. Depth of excavation [km] based on updated Fig. 22 (transient cavity) from Cintala and Grieve (1998). Reliable for craters larger than approx. 50 km
45. Depth of excavation [km] 1/10 transient cavity diameter Stöffler et al. (2006)
46. Depth of melting [km] based on updated Fig. 22 (transient cavity) from Cintala and Grieve (1998). Reliable for craters larger than approx. 50 km
47. Depth of melting [km] based on Fig. 23 (transient cavity) from Cintala and Grieve (1998). Reliable for craters between approximately 1 km and 400 km. Assumes a chondritic projectile hitting an anothosite target at 20 km/s.
48. Melt volume [km^3] calculated for chondritic projectile impacting anorthosite target at 20 km/s, based on equation 8 and Table 2 from Cintala and Grieve (1998).
49. Melt volume [km^3] calculated for a 45 degree impact on a basaltic target, using Eq. 12 and Table 1 from Abramov et al. (2012).
50. Melt volume [km^3] calculated for a 45 degree impact on an anorthositic target, using Eq. 12 and Table 1 from Abramov et al. (2012).
51. Age (Wilhelms, 1987; Wilhelms, personal communication, 2008; Wilhelms and Byrne, 2009). Note that there are discrepancies between the different sources. In most cases the most recent data has been used, see the remarks.
52. Age class (1 Pre-Nectarian, 2 Nectarian, 3 Lower Imbrian, 4 Upper Imbrian, 5 Eratosthenian, 6 Copernican) (Wilhelms, 1987; Wilhelms, personal communication, 2008; Wilhelms and Byrne, 2009)
53. Remarks (Wilhelms, 1987; Wilhelms, personal communication, 2008; Wilhelms and Byrne, 2009); Also included are the unapproved names and the former names of recently renamed craters, as well as if the crater is a small crater in Apollo or Lunokhod-1 landing sites.
54. Age - source (Wilhelms, 1987 table: 1, map only: 2; personal communication, 2008: PC; Wilhelms and Byrne, 2009: W&B09). Note that most of the ages from PC are incorporated in W&B09.
55. Age - other sources
56. Basin age group (higher number - younger) (Wilhelms, 1987)
57. Citation for first mention, citation for nomenclature (see the References worksheet) from Chuck Wood database
58. Approval year or date in the official IAU-USGS database (Gazetteer of Planetary Nomenclature)
59. Modeled central peak lithology using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
60. Modeled central peak olivine contents using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
61. Modeled central peak orthopyroxene contents using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
62. Modeled central peak clinopyroxene contents using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
63. Modeled central peak plagioclase contents using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
64. Modeled central peak combined mafics to plagioclase ratio using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
65. Modeled central peak orthopyroxene to clinopyroxene ratio using Clementine VIS-NIR data, taken from Table 3 in Cahill et al. 2009.
66. Average abundance (in weight-%, ± 1σ) of plagioclase using Kaguya Multiband Imager UVVIS/NIR data, taken from Table 1 in Lemelin et al. 2015.
67. Average abundance (in weight-%, ± 1σ) of olivine using Kaguya Multiband Imager UVVIS/NIR data, taken from Table 1 in Lemelin et al. 2015.
68. Average abundance (in weight-%, ± 1σ) of orthopyroxene using Kaguya Multiband Imager UVVIS/NIR data, taken from Table 1 in Lemelin et al. 2015.
69. Average abundance (in weight-%, ± 1σ) of clinopyroxene using Kaguya Multiband Imager UVVIS/NIR data, taken from Table 1 in Lemelin et al. 2015.
70. Average abundance (in weight-%, ± 1σ) of FeO using Kaguya Multiband Imager UVVIS/NIR data, taken from Table 1 in Lemelin et al. 2015.
71. Central peak lithologies from Table 1 in Tompkins & Pieters (1999), using Clementine UVVIS data. Abbreviations of the lithologies: A = anorthosite; GNTA1 = gabbroic-noritic-troctolitic anorthosite with 85-90% plagioclase; GNTA2 = gabbroic-noritic-troctolitic anorthosite with 80-85% plagioclase; AN = anorthositic norite; AGN = anorthositic gabbronorite; AG = anorthositic gabbro; AT = anorthositic troctolite; N = norite; GN = gabbronorite; G = gabbro; T = troctolite
72. Crater setting (highland or basin) from Table 1 in Tompkins & Pieters (1999)
73. Average location of the Christiansen feature [μm] using LRO Diviner data, from Table 2 in Song et al. 2013. For comparison: 8.67 μm -> forsterite; 8.25 μm -> intermediate pyroxene composition; 7.84 μm -> anorthite
74. Average value of the optical maturity parameter (OMAT) using Clementine data, from Table 2 in Song et al. 2013. Immature craters have OMAT > 0.3.
75. Average FeO contents [weight-%] using Clementine data, from Table 2 in Song et al. 2013.
76. The range of optical maturity parameter (OMAT) using Kaguya (SELENE) Multiband Imager (MI) data, from Supplement Table 1 in Ohtake et al. 2009.
77. Estimated modal abundance of plagioclase, presented as three classes: A (>98 ± 3% plg), B (90 to 98 ± 3% plg), and C (<90 ± 5% plg), using Kaguya (SELENE) Multiband Imager (MI) data, from Supplement Table 1 in Ohtake et al. 2009.
78. Geological province: PKT = Procellarum KREEP Terrane, SPA = South Pole - Aitken (Terrane?), or highlands (Feldspathic Highlands Terrane?), from Supplement Table 1 in Ohtake et al. 2009.
79. Central peak degradation, 1 = freshest, 4 = most degraded. From Table 1 in Donaldson Hanna et al. 2014 (they refer to Baker et al. 2012, which was not available for the database).
80. The location of the detection of spectroscopically purest anorthosite (PAN, >98 volume-%).), using Chandrayaan-1 Moon Mineralogy Mapper data, from Table 1 in Donaldson Hanna et al. 2014. CP = central peak, C = central
81. Are ejecta rays observable in visual imagery or not. Combined from a number of sources, verified and appended by T. Öhman. Original sources: Elger 1895; Grier et al. 2001; McEwen et al. 1997; Morota & Furumoto 2003; C. Wood's Moon Wiki (https://the-moon.wikispaces.com/Ray+craters)


## UAH lower troposphere

Downloaded from
http://vortex.nsstc.uah.edu/data/msu/v6.0/tlt/uahncdc_lt_6.0.txt

Long and wide versions of the same dataset.  The long dataset has
variables 

* **year**
* **month**
* **region** the temperature measures are aggregated over a number of
  different regions.  These include
    * **globe** global average temperature
    * **globe_land** average over global land area
    * **globe_ocean** average over global ocean
    * **nh**, **nh_land**, **nh_ocean**: northern hemisphere
    * **sh**, **sh_land**, **sh_ocean**
    * **trpcs**, **trpcs_land**, **trpcs_ocean**: tropics (30S .. 30N lat)
    * **noext**, **noext_land**, **noext_ocean**: northern extent
      (30N..60N latitude)
    * **soext**, **soext_land**, **soext_ocean**
    * **nopol**, **nopol_land**, **nopol_ocean**: northern polar areas
      (60N .. 90N lat)
    * **sopol**, **sopol_land**, **sopol_ocean**
    * **usa48**: lower 48
    * **usa49**: including Alaska
    * **aust**: Australia
* **temp**: temperature deviation (deg C) from 1991-2020 average (?)
