# Economics-related data

## account-transactions

1657 transactions of a bank account.

* **date**
* **amount**: in USD
* **type**: credit/debit


## benefits

From R package _Ecdat_.  It seems to be an subsample of blue-collar
workers only from McCall (1995) _The Impact of Unemployment Insurance
Benefit Levels on Recipiency_, Journal of Business & Economic
Statistics, **13**, pp 189-198.

Some of the codes are missing or not explained, e.g. what are the
state codes.  Also, logical variables are coded as "yes"/"no", not
TRUE/FALSE. 

* **stateur**: state unemployment rate (in %)
* **statemb**: state maximum benefit level
* **state**: state of residence code.  Not sure which code, it is not
  fips. 
* **age**: age in years
* **tenure**: years of tenure in job lost
* **joblost**: a factor with levels (slack\_work,position\_abolished,seasonal\_job\_ended,other)
* **nwhite**: non-white ?
* **school12**: more than 12 years of school ?
* **sex**: a factor with levels (male,female)
* **bluecol**: blue collar worker?  Only "yes" answers.
* **smsa**: lives in SMSA ?
* **married**: married ?
* **dkids**: has kids ?
* **dykids**: has young kids (0-5 yrs) ?
* **yrdispl**: year of job displacement (1982=1,..., 1991=10)
* **rr**: replacement rate
* **head**: is head of household ?
* **ui**: applied for (and received) UI benefits? ("yes"/"no")


## income-heights:

Individual data from NLSY 2002, from R package _modelr_.  NLSY
(National Longitudinal Survey of Youth) follows a birth cohort through
their life cycle, 2002 they were around 50 year old.
Each row
represents an individual (a person) as of 2002.

* **income** Yearly income. The top two percent of values were averaged
  and that average was used to replace all values in the top
  range.
* **height** Height, in inches
* **weight** Weight, in pounds
* **age** Age, in years, between 47 and 56.
* **marital** Marital status
* **sex** Sex
* **education** Years of education
* **afqt** Percentile score on Armed Forces Qualification Test.


## Diamonds

Data from ggplot2 package.  Contains prices and other attributes of almost 54,000
diamonds.

* **price**  price in US dollars (\$326--\$18,823)
* **carat**  weight of the diamond (0.2--5.01, 1 ct = 0.2g)
* **cut**  quality of the cut (Fair, Good, Very Good, Premium, Ideal)
* **color**  diamond colour, from D (best) to J (worst)
* **clarity**  a measurement of how clear the diamond is (I1 (worst), SI2, SI1, VS2, VS1, VVS2, VVS1, IF (best))
* **x**  length in mm (0--10.74)
* **y**  width in mm (0--58.9)
* **z**  depth in mm (0--31.8)
* **depth**  total depth percentage = z / mean(x, y) = 2 * z / (x + y) (43--79)
* **table**  width of top of diamond relative to widest point (43--95)


## Flights

Data about flights from NYC airports in 2013.  From R ggplot2 package.

* **year**, **month**, **day**: Date of departure.
* **dep_time**, **arr_time**: Actual departure and arrival times
  (format HHMM or HMM), local tz.
* **sched_dep_time**, **sched_arr_time**: Scheduled departure and
  arrival times (format HHMM or HMM), local tz.
* **dep_delay**, **arr_delay**: Departure and arrival delays, in
  minutes. Negative times represent early departures/arrivals.
* **carrier**: Two letter carrier abbreviation. See airlines to get name.
* **flight**: Flight number.
* **tailnum**: Plane tail number. See planes for additional metadata.
* **origin, dest**: Origin and destination. See airports for additional metadata.
* **air_time**: Amount of time spent in the air, in minutes.
* **distance**: Distance between airports, in miles.
* **hour, minute**: Time of scheduled departure broken into hour and minutes.
* **time_hour**: Scheduled date and hour of the flight as a POSIXct
  date. Along with origin, can be used to join flights data to weather
  data.


## Treatment

Originates from R package _Ecdat_.  A U.S. dataset from 1974, used
for
evaluating treatment effect of training on earnings.  

* **treat** treated ?
* **age** age
* **educ** education in years
* **ethn** a factor with levels ("‘other’", "‘black’", "‘hispanic’")
* **married** married ?
* **re74** real annual earnings in 1974 (pre-treatment)
* **re75** real annual earnings in 1975 (pre-treatment)
* **re78** real annual earnings in 1978 (post-treatment)
* **u74** unemployed in 1974 ?
* **u75** unemployed in 1975 ?

Source: Lalonde, R. (1986) “Evaluating the Econometric Evaluations of
Training Programs with Experimental Data”, _American Economic
Review_, 604-620.


## Bankruptcy in Taiwan _tw-bankruptcy.csv.bz2_

The dataset was collected from the
Taiwan Economic Journal for the years 1999 to 2009. 
Company bankruptcy
was defined based on the business regulations of the Taiwan Stock
Exchange. 

Features:

* **Bankrupt?**: Class label 0/1
* **ROA(C) before interest and depreciation before interest**: Return On Total Assets(C)
* **ROA(A) before interest and % after tax**: Return On Total Assets(A)
* **ROA(B) before interest and depreciation after tax**: Return On Total Assets(B)
* **Operating Gross Margin**: Gross Profit/Net Sales
* **Realized Sales Gross Margin**: Realized Gross Profit/Net Sales
* **Operating Profit Rate**: Operating Income/Net Sales
* **Pre-tax net Interest Rate**: Pre-Tax Income/Net Sales
* **After-tax net Interest Rate**: Net Income/Net Sales
* **Non-industry income and expenditure/revenue**: Net Non-operating Income Ratio
* **Continuous interest rate (after tax)**: Net Income-Exclude Disposal Gain or Loss/Net Sales
* **Operating Expense Rate**: Operating Expenses/Net Sales
* **Research and development expense rate**: (Research and Development Expenses)/Net Sales
* **Cash flow rate**: Cash Flow from Operating/Current Liabilities
* **Interest-bearing debt interest rate**: Interest-bearing Debt/Equity
* **Tax rate (A)**: Effective Tax Rate
* **Net Value Per Share (B)**: Book Value Per Share(B)
* **Net Value Per Share (A)**: Book Value Per Share(A)
* **Net Value Per Share (C)**: Book Value Per Share(C)
* **Persistent EPS in the Last Four Seasons**: EPS-Net Income
* **Cash Flow Per Share**
* **Revenue Per Share (Yuan ¥)**: Sales Per Share
* **Operating Profit Per Share (Yuan ¥)**: Operating Income Per Share
* **Per Share Net profit before tax (Yuan ¥)**: Pretax Income Per Share
* **Realized Sales Gross Profit Growth Rate**
* **Operating Profit Growth Rate**: Operating Income Growth
* **After-tax Net Profit Growth Rate**: Net Income Growth
* **Regular Net Profit Growth Rate**: Continuing Operating Income after Tax Growth
* **Continuous Net Profit Growth Rate**: Net Income-Excluding Disposal Gain or Loss Growth
* **Total Asset Growth Rate**: Total Asset Growth
* **Net Value Growth Rate**: Total Equity Growth
* **Total Asset Return Growth Rate Ratio**: Return on Total Asset Growth
* **Cash Reinvestment %**: Cash Reinvestment Ratio
* **Current Ratio**
* **Quick Ratio**: Acid Test
* **Interest Expense Ratio**: Interest Expenses/Total Revenue
* **Total debt/Total net worth**: Total Liability/Equity Ratio
* **Debt ratio %**: Liability/Total Assets
* **Net worth/Assets**: Equity/Total Assets
* **Long-term fund suitability ratio (A)**: (Long-term Liability+Equity)/Fixed Assets
* **Borrowing dependency**: Cost of Interest-bearing Debt
* **Contingent liabilities/Net worth**: Contingent Liability/Equity
* **Operating profit/Paid-in capital**: Operating Income/Capital
* **Net profit before tax/Paid-in capital**: Pretax Income/Capital
* **Inventory and accounts receivable/Net value**: (Inventory+Accounts Receivables)/Equity
* **Total Asset Turnover**
* **Accounts Receivable Turnover**
* **Average Collection Days**: Days Receivable Outstanding
* **Inventory Turnover Rate (times)**
* **Fixed Assets Turnover Frequency**
* **Net Worth Turnover Rate (times)**: Equity Turnover
* **Revenue per person**: Sales Per Employee
* **Operating profit per person**: Operation Income Per Employee
* **Allocation rate per person**: Fixed Assets Per Employee
* **Working Capital to Total Assets**
* **Quick Assets/Total Assets**
* **Current Assets/Total Assets**
* **Cash/Total Assets**
* **Quick Assets/Current Liability**
* **Cash/Current Liability**
* **Current Liability to Assets**
* **Operating Funds to Liability**
* **Inventory/Working Capital**
* **Inventory/Current Liability**
* **Current Liabilities/Liability**
* **Working Capital/Equity**
* **Current Liabilities/Equity**
* **Long-term Liability to Current Assets**
* **Retained Earnings to Total Assets**
* **Total income/Total expense**
* **Total expense/Assets**
* **Current Asset Turnover Rate**: Current Assets to Sales
* **Quick Asset Turnover Rate**: Quick Assets to Sales
* **Working capitcal Turnover Rate**: Working Capital to Sales
* **Cash Turnover Rate**: Cash to Sales
* **Cash Flow to Sales**
* **Fixed Assets to Assets**
* **Current Liability to Liability**
* **Current Liability to Equity**
* **Equity to Long-term Liability**
* **Cash Flow to Total Assets**
* **Cash Flow to Liability**
* **CFO to Assets**
* **Cash Flow to Equity**
* **Current Liability to Current Assets**
* **Liability-Assets Flag**: 1 if Total Liability exceeds Total Assets, 0 otherwise
* **Net Income to Total Assets**
* **Total assets to GNP price**
* **No-credit Interval**
* **Gross Profit to Sales**
* **Net Income to Stockholder's Equity**
* **Liability to Equity**
* **Degree of Financial Leverage (DFL)**
* **Interest Coverage Ratio (Interest expense to EBIT)**
* **Net Income Flag**: 1 if Net Income is Negative for the last two years, 0 otherwise
* **Equity to Liability**
