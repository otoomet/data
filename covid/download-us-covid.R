#!/usr/bin/env Rscript
### Download covid case dataset from John Hopkins COVID-19 data repo
### https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series
### save it in the original form, and also a smaller subset for the lab
### 
library(data.table)
library(magrittr)

source("../bin/utils.R")

args <- argparser::arg_parser("Download the US covid death data") %>%
   argparser::add_argument("--build-date", "date in output file names", format(Sys.Date())) %>%
   argparser::add_argument("--no-download",
                           "do not download, load the local data file", flag=TRUE) %>%
   argparser::parse_args()
##
fileDate <- args$build_date
outFName <- paste0("time-series-covid19-deaths-us_", fileDate, ".csv.bz2")
reducedFName <- paste0("time-series-covid19-deaths-us-reduced_", fileDate, ".csv.bz2")
## download
if(!args$no_download) {
   dataUrl <-
      "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv"
   cat("\nDownload covid data from", dataUrl, "\n")
   df <- fread(dataUrl)
   writeTable(df, outFName)
   system(paste("ln -sf", outFName, "time-series-covid19-deaths-us.csv.bz2"))
   cat("data saved\n")
} else {
   cat("Load local data file\n")
   df <- fread(outFName)
}
##
dateCols <- grep("^[[:digit:]]+/[[:digit:]]+/[[:digit:]]+$", names(df), value=TRUE)
dates <- dateCols %>%
   as.Date(format="%m/%d/%Y") %>%
   sort()
cat("The earliest date:", format(dates[1]),
    "\nmost recent date:", format(tail(dates, 1)), "\n")
firstDates <- head(dates, 5)
lastDates <- tail(dates, 5)
rndDates <- sample(dates, 5)
keepDates <- c(firstDates, lastDates, rndDates) %>%
   unique() %>%
   sort() %>%
   format("%m/%d/%y") %>%
   gsub("(^|/)0", "\\1", .)
                           # remove leading zeros in day/month
delCols <- c(setdiff(dateCols, keepDates),
           c("iso3", "code3", "Country_Region"))
df[, (delCols) := NULL]
cat("New reduced data frame ", nrow(df), "x", ncol(df), "\n", sep="")
writeTable(df, reducedFName)
system(paste("ln -sf", reducedFName, "time-series-covid19-deaths-us-reduced.csv.bz2"))
cat("All done :-)\n")
