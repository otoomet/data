# COVID-19 datasets

## code

* `covid.R`: download covid data from 
  "https://raw.githubusercontent.com/datasets/covid-19/master/data/time-series-19-covid-combined.csv".
  Separate Scandinavian dataset from it, add (2020) population to it,
  and compute case growth and growth per capita.
  

## Global covid data _covid-global_mm-dd-yyy.csv_

Downloaded from [John Hopkins COVID-19 data
repo](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data),
individual file urls are like
_https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/02-01-2020.csv_. 

Documentation from the source repo:

### Field description
* **FIPS**: US only. Federal Information Processing Standards code that uniquely identifies counties within the USA.
* **Admin2**: County name. US only.
* **Province_State**: Province, state or dependency name.
* **Country_Region** (**Country/Region** in some data files):
  Country, region or sovereignty name. The names
  of locations included on the Website correspond with the official
  designations used by the U.S. Department of State.
* **Last Update**: MM/DD/YYYY HH:mm:ss  (24 hour format, in UTC).
* **Lat** and **Long_**: Dot locations on the dashboard. All points
  (except for Australia) shown on the map are based on geographic
  centroids, and are not representative of a specific address,
  building or any location at a spatial scale finer than a
  province/state. Australian dots are located at the centroid of the
  largest city in each state.
* **Confirmed**: Counts include confirmed and probable (where reported).
* **Deaths**: Counts include confirmed and probable (where reported).
* **Recovered**: Recovered cases are estimates based on local media
  reports, and state and local reporting when available, and therefore
  may be substantially lower than the true number. US state-level
  recovered cases are from [COVID Tracking
  Project](https://covidtracking.com/). We stopped to maintain the
  recovered cases (see [Issue
  #3464](https://github.com/CSSEGISandData/COVID-19/issues/3464) and
  [Issue
  #4465](https://github.com/CSSEGISandData/COVID-19/issues/4465)).
* **Active:** Active cases = total cases - total recovered - total
  deaths. This value is for reference only after we stopped to report
  the recovered cases (see [Issue
  #4465](https://github.com/CSSEGISandData/COVID-19/issues/4465))
* **Incident_Rate**: Incidence Rate = cases per 100,000 persons.
* **Case_Fatality_Ratio (%)**: Case-Fatality Ratio (%) = Number
  recorded deaths / Number cases.

All cases, deaths, and recoveries reported are based on the date of
initial report. Exceptions to this are noted in the "Data
Modification" and "Retrospective reporting of (probable) cases and
deaths" subsections below.

### Update frequency
* Since June 15, We are moving the update time forward to occur
  between 04:45 and 05:15 GMT to accommodate daily updates from
  India's Ministry of Health and Family Welfare.
* Files on and after April 23, once per day between 03:30 and 04:00 UTC. 
* Files from February 2 to April 22: once per day around 23:59 UTC.
* Files on and before February 1: the last updated files before 23:59
  UTC. Sources:
  [archived_data](https://github.com/CSSEGISandData/COVID-19/tree/master/archived_data)
  and dashboard.

### Data sources
Refer to the [mainpage](https://github.com/CSSEGISandData/COVID-19).


## US covid deaths: _time-series-covid19-deaths-us.csv.bz2_

Downloaded 2021-01-10 from
[John Hopkins COVID-19 data
repo](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series)

Use `download-us-covid.R` to download the most recent dataset.  The
dataset will be saved as `time-series-covid19-deaths-us.csv.bz2`
(symlink to the most recent dated file).  It also creates a smaller
version with only the first, last, and a few other dates retained,
`time-series-covid19-deaths-us-reduced.csv.bz2`. 

\item[UID] Similar to Admin2, but includes locations like Diamond
  Princess, unassigned, etc. (Not needed for this activity)
\item["6/16/2020"] total number of confirmed COVID-19 deaths as of
  June 16, 2020

* **UID** Similar to _Admin2_, but includes locations like Diamond
  Princess, unassigned, etc.
* **iso2**: iso 3166-1 country code
* **FIPS**: US only. Federal Information Processing Standards code
  that uniquely identifies counties within the USA.  For instance,
  King County has fips code 53033 where ``53'' stands for
  Washington.
* **Admin2**: County name
* Province_State
* Lat
* Long_
* **Combined_Key**: Combined county-state-country name
* Population
* **1/22/20** and other similar: number of COVID-19 confirmed deaths
  at this date.


## covid-scandinavia

Data dowloaded from [github
data](https://raw.githubusercontent.com/datasets/covid-19/master/data/time-series-19-covid-combined.csv)

Extracted a subset of Scandinavian countries, only national level,
only deaths, confirmed cases.  Added per capita counts and daily
growth numbers.

Variables:

* **code2**: 2-letter country code
* **country**: country name
* **state**: federal state, just _NA_ in case of Scandinavian countries 
* **date**: date of the count
* **type**: count type
* **count**: how many persons have confirmed covid/died
* **lockdown**: date where major lockdowns began
* **population**: country population (only one number)
* **countPC**: count per capita
* **growth**: growth in count 
* **growthPC**: growth in count per capita
