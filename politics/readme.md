# Data about politics

## Democracy

Dowloaded from https://github.com/rfordatascience/tidytuesday/blob/main/data/2024/2024-11-05/democracy_data.csv

Codebook from the same website:

|variable                    |class     |description                           |
|:---------------------------|:---------|:-------------------------------------|
|country                     |character |Country name in the original PACL dataset. |
|iso3                        |character |Three letter ISO country code. |
|year                        |integer   |Year. |
|regime_category_index       |integer   |Numeric regime category, following Cheibub, Ghandi and Vreeland (2010). |
|regime_category             |character |Regime category label, following Cheibub, Ghandi and Vreeland (2010). |
|is_monarchy                 |logical   |Is the country a hereditary monarchy? |
|is_commonwealth             |logical   |Is the country a member of the British Commonwealth? |
|monarch_name                |character |Name of the monarch. |
|monarch_accession_year      |integer   |Year of accession of the monarch. |
|monarch_birthyear           |integer   |Year of birth of the monarch. |
|is_female_monarch           |logical   |Is the monarch female. |
|democracy                   |logical   |Is the country democratic or not? Following Cheibub, Ghandi and Vreeland (2010) Dichotomous indicator of democracy based on a minimalist definition. A country is defined as democratic, if elections were conducted, these were free and fair, and if there was a peaceful turnover of legislative and executive offices following those elections. |
|is_presidential             |logical   |Is the political system presidential? |
|president_name              |character |Name of the president. |
|president_accesion_year     |integer   |Accession year of the president. |
|president_birthyear         |integer   |Year of birth of the president. |
|is_interim_phase            |logical   |Is the president interim / preliminary? (more than 2 Presidents/year) |
|is_female_president         |logical   |Is the president female? |
|is_colony                   |logical   |Is the country a colony? |
|colony_of                   |character |If colony, which country is the colonial power? Country name of the colonial power. |
|colony_administrated_by     |character |If colony, which country is the colonial administrator? |
|is_communist                |logical   |Is the country's regime communist / socialist? |
|has_regime_change_lag       |logical   |Regime Change lag. If a coded event, such as a change in the Presidency, took place after 01.07 it is assigned to the following calendar year in the data. In this case, the lag variable will be TRUE. For all change events before that date, the lag variable is FALSE. |
|spatial_democracy           |double    |Average of geographical neighbors' Democracy score. |
|parliament_chambers         |integer   |Total number of chambers in parliament. |
|has_proportional_voting     |logical   |Is the electoral system characterized by including proportional representation? |
|election_system             |character |Electoral system. See [the package website](https://xmarquez.github.io/democracyData/reference/pacl_update.html) for a full list of options. |
|lower_house_members         |integer   |If bicameral parliament, total number of members in lower house. |
|upper_house_members         |integer   |If bicameral parliament, total number of members in upper house. |
|third_house_members         |integer   |If tricameral parliament, total number of members in third house. |
|has_new_constitution        |logical   |Whether a new constitution was implemented. |
|has_full_suffrage           |logical   |Whether electoral system attributes full suffrage. |
|suffrage_restriction        |character |If no full suffrage, kind of suffrage restriction. |
|electoral_category_index    |integer   |Alternative democracy indicator capturing degree of multi-party competition (index from 0 to 3). |
|electoral_category          |character |Alternative democracy indicator capturing degree of multi-party competition. |
|spatial_electoral           |double    |Average of geographical neighbors' electoral_category_index. |
|has_alternation             |logical   |Whether there's an alternation in power after election. Undocumented in original codebook. |
|is_multiparty               |logical   |Whether the elections are multiparty. Undocumented in original codebook. |
|has_free_and_fair_election  |logical   |Whether the elections are free and fair. Undocumented in original codebook. |
|parliamentary_election_year |integer   |Year of parliamentary election. Undocumented in original codebook. |
|election_month              |character |Month of parliamentary election. Undocumented in original codebook. |
|election_year               |integer   |Year of parliamentary election. Undocumented in original codebook. |
|has_postponed_election      |logical   |Whether the election was postponed. Undocumented in original codebook. |


## House votes

Votes by 435 Representatives of the U.S. House of Representatives on
16 bills.  There are two versions of the file:

* **house-votes-84.csv**: does not contain variable names, and
  includes three types of data: _y_ for _yea_, _n_ for _nay_ and _?_
  for something else (such as missing vote)
* **house-votes-84-years.csv**: simplified version of the same data,
  it includes variable names _party_ for political party, and
  _yea1_-_yea16_ for votes for the bills.  The voting data is coded as _1_
  for _yea_ and _0_ for something else (usually _nay_).

### Original data description

2. Source Information:
    * Source: Congressional Quarterly Almanac, 98th Congress, 2nd
                 session 1984, Volume XL: Congressional Quarterly Inc.
                 Washington, D.C., 1985.
    * Donor: Jeff Schlimmer (Jeffrey.Schlimmer@a.gp.cs.cmu.edu)
    * Date: 27 April 1987 
4. Relevant Information:
      This data set includes votes for each of the U.S. House of
      Representatives Congressmen on the 16 key votes identified by the
      CQA.  The CQA lists nine different types of votes: voted for, paired
      for, and announced for (these three simplified to yea), voted
      against, paired against, and announced against (these three
      simplified to nay), voted present, voted present to avoid conflict
      of interest, and did not vote or otherwise make a position known
      (these three simplified to an unknown disposition).
5. Number of Instances: 435 (267 democrats, 168 republicans)
7. Attribute Information:

    1. Class Name: 2 (democrat, republican)
    2. handicapped-infants: 2 (y,n)
    3. water-project-cost-sharing: 2 (y,n)
    4. adoption-of-the-budget-resolution: 2 (y,n)
    5. physician-fee-freeze: 2 (y,n)
    6. el-salvador-aid: 2 (y,n)
    7. religious-groups-in-schools: 2 (y,n)
    8. anti-satellite-test-ban: 2 (y,n)
    9. aid-to-nicaraguan-contras: 2 (y,n)
    10. mx-missile: 2 (y,n)
    11. immigration: 2 (y,n)
    12. synfuels-corporation-cutback: 2 (y,n)
    13. education-spending: 2 (y,n)
    14. superfund-right-to-sue: 2 (y,n)
    15. crime: 2 (y,n)
    16. duty-free-exports: 2 (y,n)
    17. export-administration-act-south-africa: 2 (y,n)



## US presidents' approval

Data about presidents' approval rate (from Gallup)

* [Donald
  Trump](https://news.gallup.com/poll/203198/presidential-approval-ratings-donald-trump.aspx)
* [Barack Obama](https://news.gallup.com/poll/116479/barack-obama-presidential-job-approval.aspx)

Variables:

* **date**: original date in data (date range for the poll)
* **approve**: approval rate (pct)
* **disapprove**: disapproval rate (pct)
* **dontknow**: don't know rate
* **president**
* **meandate**: mean date for the poll date range
* **officeday**: days since president's inauguration

