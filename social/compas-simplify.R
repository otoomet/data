#!/usr/bin/env Rscript
library(tidyverse)
source("../bin/utils.R")

compas <- read_delim("compas-score-two-years-reduced-370.csv.bz2") %>%
   mutate(risk = as.numeric(decile_score > 5),
          prior = priors_count > 0) %>%
   select(risk, recidivist = two_year_recid,
          age, prior, sex, race) %>%
   filter(race %in% c("Caucasian", "African-American"))

cat("Dataset of", nrow(compas), "rows and", ncol(compas), "columns\n")
cat("Data example:\n")
compas %>%
   sample_n(5) %>%
   print()

writeTable(compas, "compas-binary.csv.bz2")
cat("All done!\n")
