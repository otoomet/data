# Housing related data

## airbnb-bangkok-listings.csv.bz2

Downloaded from [Inside
AirBnB](http://insideairbnb.com/get-the-data/).  There is also some
documentation.  

## airbnb-bangkok-score.csv.bz2

Data to demonstrate overfitting through random columns.

Compiled from _airbnb-bangkok-listings.csv.bz2_ using
_review-score.R_.  This extracts a few selected variables from the
original data, samples 2000 cases from there,
and creates a new column _highScore_, which is true if
the reviews score is above the median (4.79).  It also does a few more
cleaning.

Besides of that, it adds 30 columns of random character.


## king-house-prices

From Kaggle, origin not clear, maybe from https://info.kingcounty.gov/assessor/esales/Residential.aspx

* **Id**: Unique ID for each home sold
* **Date**: Date of the home sale
* **Price**: Price of each home sold
* **Bedrooms**: Number of bedrooms
* **Bathrooms**: Number of bathrooms, where .5 accounts for a room with a toilet but no shower
* **Sqft_living**: Square footage of the apartments interior living space
* **Sqft_lot**: Square footage of the land space
* **Floors**: Number of floors
* **Waterfront**: A dummy variable for whether the apartment was overlooking the waterfront or not
* **View**: An index from 0 to 4 of how good the view of the property was
* **Condition**: An index from 1 to 5 on the condition of the apartment,
* **Grade**: An index from 1 to 13, where 1-3 falls short of building construction and design, 7 has an average level of construction and design, and 11- 13 have a high quality level of construction and design
* **Sqft_above**: The square footage of the interior housing space that is above ground level
* **Sqft_basement**: The square footage of the interior housing space that is below ground level
* **Yr_built**: The year the house was initially built
* **Yr_renovated**: The year of the house’s last renovation
* **Zipcode**: What zipcode area the house is in
* **Lat**: Latitude
* **Long**: Longitude
* **Sqft_living15**: The square footage of interior housing living space for the nearest 15 neighbors
* **Sqft_lot15**: The square footage of the land lots of the nearest 15 neighbors
