# Social data

## Babynames

Extracted from R _babynames_ package, collected by the U.S. Social
Security Administration (?, by SSA).

* **year**
* **sex**
* **name**
* **n**
* **prop**: proportion of babies of this sex given this name this year.

## Boston

Housing Values in Suburbs of Boston, extracted from MASS package

* **crim** per capita crime rate by town.
* **zn** proportion of residential land zoned for lots over 25,000
    sq.ft.
* **indus** proportion of non-retail business acres per town.
* **chas** Charles River dummy variable (= 1 if tract bounds river; 0
    otherwise).
* **nox** nitrogen oxides concentration (parts per 10 million).
* **rm** average number of rooms per dwelling.
* **age** proportion of owner-occupied units built prior to 1940.
* **dis** weighted mean of distances to five Boston employment
    centres.
* **rad** index of accessibility to radial highways.
* **tax** full-value property-tax rate per \$10,000.
* **ptratio** pupil-teacher ratio by town.
* **black** 1000(Bk - 0.63)^2 where Bk is the proportion of blacks by
    town.
* **lstat** lower status of the population (percent).
* **medv** median value of owner-occupied homes in \$1000s.


## College Admissions

Data from
[Kaggle](https://www.kaggle.com/mohansacharya/graduate-admissions/version/2).
It refers to a paper "Mohan S Acharya, Asfia Armaan, Aneeta S Antony :
A Comparison of Regression Models for Prediction of Graduate
Admissions, IEEE International Conference on Computational
Intelligence in Data Science 2019", and there is another reference to
it, [Predicting Student University Admission Using Logistic
Regression](https://www.eajournals.org/wp-content/uploads/Predicting-Student-University-Admission-Using-Logistic-Regression.pdf).
However neither of the papoers actually discusses the data.  So we do
not know what are the variables/cases.  Is it about individual
students?  But how can they have admission probability?  Is it about
departments/schools?  But what does GRE score and such mean?  Is it
actual data or some sort of synthetic data?

There are no missings.

Variables:

* **Serial No.**:
* **GRE Score**: (out of 340)
* **TOEFL Score**: (out of 120)
* **University Rating**: (out of 5)
* **SOP**: statement of purpose (out of 5).
* **LOR**: letter of recommendation (out of 5)
* **CGPA**: undergraduate GPA (out of 10)
* **Research**: 0/1 for research experience
* **Chance of Admit**:, admission probability


## Compas-binary

A simplified COMPAS dataset with only binary predictions

Variables:

* **risk**: "1" likely to be recidivist (COMPAS score > 5),
  "0" not likely (score 5 or less)
* **recidivist**: whether the person re-offended ("1") or not ("0")
* **age**
* **prior**: "TRUE": has prior criminal record, "FALSE": does not have
* **sex**: "Male"/"Female"
* **race**: "Caucasian"/"African-American"



## Disasters

Information scraped from FEMA website about declared emergencies

* **Number**: FEMA incidence id
* **State.Tribal.Government**: region (state or tribal gvt) where the
  emergency was declared.
* **Incident.Description**: a few word description of the incident
* **Declaration.Type**: type of the emergency declaration
* **date**: date of the emergency declaration (may be quite different
  from the incident dates)
* **news**: number of official media articles (not the count of
  mainstream media attention)
* **nApplications**: individual assitance applications approved
* **indTotal**: total individual assistance (USD approved)
* **housing**: total housing assitance (USD approved)
* **other**: total other needs assitance (USD approved)
* **pubTotal**: total public assistance dollars obliged
* **emergency**: public assistance for emergency work (USD obligated)
* **permanent**: public assistance for permanent work (USD obligated)

The original code is `scrape-emergencies.R`.  But that file is
orginally not designed for just this task but to include more files,
it should be adapted.


## firstnames: First name and Race

Frequency of different races based on the first name.  Based on
**Tzioumis, K.** _Demographic aspects of first names_,
_Scientific Data_, 2018, 5, 180025.  Data is compiled from load
applications in the US and contains 4250 different first names,
distributed in 6 racial groups.

Data citation is **Tzioumis,
Konstantinos**, 2018, _Data for: Demographic aspects of first names_, 
https://doi.org/10.7910/DVN/TYJKEZ, Harvard Dataverse, V1,
UNF:6:5PcFwvADtKPydVpPOelYPg== [fileUNF]

Variables:
**obs**	Number of occurrences in the combined mortgage datasets
**pcthispanic**	Percent Hispanic or Latino
**pctwhite**	Percent Non-Hispanic White
**pctblack**	Percent Non-Hispanic Black or African American
**pctapi**	Percent Non-Hispanic Asian or Native Hawaiian or Other Pacific Islander
**pctaian**	Percent Non-Hispanic American Indian or Alaska Native
**pct2prace**	Percent Non-Hispanic Two or More Races

## NY Population

Downloaded from [NY Dept of
Health](https://www.health.ny.gov/statistics/vital_statistics/2010/table01.htm)
website.  Note: it is about the state, not about NYC only, there is
also data about NYC and about the state w/o NYC.  These are not here.


## NYPD Shooting Incidents

Downloaded from [NY Open
Data](https://data.cityofnewyork.us/Public-Safety/NYPD-Shooting-Incident-Data-Historic-/833y-fsy8).

The original data description (in
https://data.cityofnewyork.us/api/views/833y-fsy8/files/f5f61d94-6961-47bd-8d3c-e57ebeb4cb55?download=true&filename=NYPD_Shootings_Historic_DataDictionary.xlsx): 

This is a breakdown of every shooting incident that occurred in NYC
going back to 2013 through the end of the previous calendar year.
This data is manually extracted every quarter and reviewed by the
Office of Management Analysis and Planning before being posted on the
NYPD website.  Each record represents a shooting incident in NYC and
includes information abot the event, the location and time of
occurrence.  In addition, information related to suspect and victim
demographics is also included.

Variables:

* **INCIDENT_KEY** Randomly generated persistent ID for each arrest,
  Plain Text
* **OCCUR_DATE** Exact date of the shooting incident, Date & Time
* **OCCUR_TIME** Exact time of the shooting incident, Plain Text
* **BORO** Borough where the shooting incident occurred, Plain Text
* **PRECINCT** Precinct where the shooting incident occurred Number
* **JURISDICTION_CODE** Jurisdiction where the shooting incident
  occurred. Jurisdiction codes 0(Patrol), 1(Transit) and 2(Housing)
  represent NYPD whilst codes 3 and more represent non NYPD
  jurisdictions, Number
* **LOCATION_DESC** Location of the shooting incident, Plain Text
* **STATISTICAL_MURDER_FLAG**	Shooting resulted in the victim’s death which
would be counted as a murder, Checkbox
* **PERP_AGE_GROUP** Perpetrator’s age within a category, Plain Text
* **PERP_SEX** Perpetrator’s sex description Plain Text
* **PERP_RACE** Perpetrator’s race description Plain Text
* **VIC_AGE_GROUP,** Victim’s age within a category Plain Text
* **VIC_SEX** Victim’s sex description, Plain Text
* **VIC_RACE** Victim’s race description Plain Text
* **X_COORD_CD** Midblock X-coordinate for New York State Plane
Coordinate System, Long Island Zone, NAD 83, units feet (FIPS 3104),
Plain Text
* **Y_COORD_CD** Midblock Y-coordinate for New York State Plane
Coordinate System, Long Island Zone, NAD 83, units feet (FIPS 3104),
Plain Text
* **Latitude** Latitude coordinate for Global Coordinate System, WGS 1984,
decimal degrees (EPSG 4326), Number
* **Longitude** Longitude coordinate for Global Coordinate System, WGS 1984,
decimal degrees (EPSG 4326), Number
* **Lon_Lat** Longitude and Latitude Coordinates for mapping, Point

The website lists the following "footnotes":

1. Information is accurate as of the date it was queried from the
  system of record, but should be considered a close approximation of
  current records, due to revisions and updates.
2. Data is available as of the date that technological enhancements to
  information systems allowed for data capture. Null values appearing
  frequently in certain fields may be attributed to changes on
  official department forms where data was previously not
  collected. Null values may also appear in instances where
  information was not available or unknown at the time of the report
  and should be considered as either “Unknown/Not Available/Not
  Reported.”
3. A shooting incident can have multiple victims involved and as a
  result duplicate INCIDENT_KEY’s are produced. Each INCIDENT_KEY
  represents a victim but similar duplicate keys are counted as one
  incident.
4. Shooting incidents occurring near an intersection are represented
  by the X coordinate and Y coordinates of the intersection Shooting
  incidents occurring anywhere other than at an intersection are
  geo-located to the middle of the nearest street segment where
  appropriate.
5. Any attempt to match the approximate location of the incident to an
  exact address or link to other datasets is not recommended.
6. Many other shooting incidents that were not able to be geo-coded
  (for example, due to an invalid address) have been located as
  occurring at the police station house within the precinct of
  occurrence.
7. Shooting incidents occurring in open areas such as parks or beaches
  may be geo-coded as occurring on streets or intersections bordering
  the area.
8. Shooting incidents occurring on a moving train on transit systems
  are geo-coded as occurring at the train’s next stop.
9. All shooting incidents occurring within the jurisdiction of the
  Department of Correction have been geo-coded as occurring on Riker’s
  Island.
10. X and Y Coordinates are in NAD 1983 State Plane New York Long
  Island Zone Feet (EPSG 2263).
11. Latitude and Longitude Coordinates are provided in Global
  Coordinate System WGS 1984 decimal degrees (EPSG 4326).12. Errors in
  data transcription may result in nominal data inconsistencies.
14. Only valid shooting incidents resulting in an injured victim are
  included in this release. Shooting incidents not resulting in an
  injured victim are classified according to the appropriate offense
  according to NYS Penal Law.


## Professors' Salaries

Extracted from R _carData_ package

The 2008-09 nine-month academic salary for Assistant Professors,
Associate Professors and Professors in a college in the U.S. The data
were collected as part of the on-going effort of the college's
administration to monitor salary differences between male and female
faculty members. 

* Format: A csv file with 397 observations on the following 6 variables.
* rank: a factor with levels AssocProf AsstProf Prof
* discipline: a factor with levels A (“theoretical” departments) or B (“applied” departments).
* yrs.since.phd: years since PhD.
* yrs.service: years of service.
* sex: a factor with levels Female Male
* salary: nine-month salary, in dollars.

References: Fox J. and Weisberg, S. (2011) An R Companion to Applied Regression,
Second Edition Sage.


## ukraine-violence-fatalities.csv.bz2

A weekly dataset providing the total number of reported political
violence events and fatalities in Ukraine broken down by
month. Political violence events include ACLED’s battles, violence
against civilians, and explosions/remote violence event types, as well
as the mob violence sub-event type of the riots event type. 

**Terms of use**:
All rights to use or exploit ACLED (Armed Conflict Location & Event
Data Project)
data or analysis are conditioned on
the user’s adherence to the Attribution Policy (outlined here:
https://acleddata.com/download/13233/).

No user is permitted to (i) use ACLED’s data or analysis in any manner
that may harm, target, oppress, or defame ACLED, the data subjects, or
any group or population, or cause any of the foregoing to be harmed,
targeted, oppressed, or defamed; (ii) provide, permit or allow direct
access to any of ACLED’s original/raw data or analysis; (iii) use any
of the data or analysis to create, develop, support, or provide
benchmarking for any dataset, product, or platform similar to, or in
competition with, or would create a functional substitute for, any of
ACLED’s content, products, or platform; and (iv) provide, permit, or
allow access to any of the data or analysis by ACLED's competitors.


Downloaded from
https://data.humdata.org/dataset/ukraine-acled-conflict-data




## WVS Satisfaction Subset

Life satisfaction-related questions from World Value Surve v6.

* **satisfaction** (V23): "All things considered, how satisfied are you with your life as a whole these days? Using this card on
  which 1 means you are “completely dissatisfied” and 10 means you are “completely satisfied”
  where would you put your satisfaction with your life as a whole? (Code one number):

* **marital** (V57) "Are you currently (read out and code one answer
  only):
    1 Married
    2 Living together as married
    3 Divorced
    4 Separated
    5 Widowed
    6 Single

* **income** (V239): On this card is an income scale on which 1 indicates the lowest income group and 10 the highest
  income group in your country. We would like to know in what group your household is. Please,
  specify the appropriate number, counting all wages, salaries, pensions and other incomes that come
  in. (Code one number):
    1: Lowest group ... 10: Highest group

* **age** (V242) (years)

* **religious** (V147): "Independently of whether you attend religious services or not, would you say you are
  (read out and code one answer):
    1 A religious person
    2 Not a religious person
    3 An atheist

* **religionRules** (V153): "Please tell us if you strongly agree, agree, disagree, or strongly disagree with the following statements:
  Whenever science and religion conflict, religion is always right
  1: strongly agree ... 4 strongly disagree

* **armyRules** (V127): "I'm going to describe various types of political systems and ask what you think about each as a way of
  governing this country. For each one, would you say it is a very good, fairly good, fairly bad or very bad
  way of governing this country?"
  Having the army rule
  1: very good ... 4: very bad

* **continent** (based on region)

* **region** (V2) (mostly country codes, but also include dissolved
  countries and other regional units)

In all cases negative numbers mean missing answers (don't know, refuse
to answer, not applicable, ...).  In certain cases '9' or '99' also
mean missings.


## WVS PCA subset

motherWorks: when a mother works for pay, children suffer.
  1: strongly agree ... 4: strongly disagree

maleLeaders: on the whole, men make better political leaders
  than woman do
  1: strongly agree ... 4: strongly disagree

collegeBoy: a university education is more important for a
  boy than for a girl
  1: strongly agree ... 4: strongly disagree

maleDecisions: on the whole, men make better business
  executives than women do
  1: strongly agree ... 4: strongly disagree

housewife: being a housewife is just as fulfilling as
  working for pay
  1: strongly agree ... 4: strongly disagree

environmentImportant: looking after after the environment is
  important to that person; to care for nature and save
  life resourses
  1: very much like me ... 6: not at all like me

traditionImportant: Tradition is important to this person; to follow
  the customs handed down by one's religion or family.
  1: very much like me ... 6: not at all like me

trustAliens: whether
  you trust people of another nationality completely, somewhat,
  not very much or not at all?
  1: trust completely ... 4: do not trust at all

trustArmy: could you tell me how much confidence you
  have in the armed forces: is it a great deal of confidence, quite a
  lot of confidence, not very much confidence or none at
  all?
  1: a great deal ... 4: none at all

strongLeader: would you say it is a very good, fairly good, fairly bad or
  very bad way of governing this country to have a strong leader who does not have
  to bother with parliament and elections
  1: very good ... 4: very bad

religionRules: Whenever science and religion conflict, religion
  is always right.
  1: strongly agree ... 4: strongly disagree

onlyMyReligion: The only acceptable religion is my religion.
  1: strongly agree ... 4: strongly disagree

continent: "Africa"   "Asia"     "Europe"   "Americas" ""

region: numeric country/region code


