The data comes in the form of Zagat
reviews from 168 Italian restaurants in New York City from 2001.  The
original version contains _Price_, the logit version _Cheap_.

* _Restaurant_: restaurants name
* _Price_: meal price
* _Cheap_: is the restaurant considered cheap (1) or
  expensive (0)
* _Food_, _Decor_, _Service_: rating (0...30) for these items, more
  is better
* East: restaurant located east of the 5th Ave.

