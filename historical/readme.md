# Historical datasets

## Portuguese sea battles

Got it from Chirag.  Description from
[Chegg](https://www.chegg.com/homework-help/exercise-use-portuguese-sea-battles-data-oa-928-contains-out-chapter-9-problem-16hop-solution-9781108673907-exc): 

Contains the outcomes of naval battles between Portuguese and
Dutch/British ships between 1583 and 1663.

* **battle**: Name of the battle place
* **year**: Year of the battle
* **PTships**: Number of Portuguese ships
* **NLships**: Number of Dutch ships
* **ENships**: Number of ships from English side
* **ratio of Portuguese to Dutch/British ships
* **ESinvolvement**: 1 = Yes, 0 = No
* **outcome**: ‒1 = Defeat, 0 = Draw, +1 = Victory


## Titanic
