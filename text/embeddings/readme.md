# Embeddings: pre-trained word embeddings

## glove.glove.840B.300d.txt.bz2

Downloaded from https://nlp.stanford.edu/projects/glove/.  300-D glove
embeddings of 840G common crawl tokens.


## glove.twitter.27B.50d.txt.bz2

Dowloaded from https://nlp.stanford.edu/projects/glove/.  50-D glove
vectors from 2B tweets, 27B tokens, 1.2M vocab, uncased.  There are
also 25, 100, and 200d vectors.

Also included a subsample for ~20k most frequent words 


## country-concept-similarity

Cosine similarity between the embedding vectors of concepts (such as
_terrorism_, _trade_, _palm_) and country name.  Embeddings are the
300-D common crawl 840G token vectors, scraped ~2015.  Country names
are from ggplot's world map.  As the embeddings do not handle
multi-word country names, these are left missing.

Included code to compute these similarities.
