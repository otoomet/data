#!/usr/bin/env Rscript
### Read the lingspam_public.tar.gz and create a data frame
### from the bare text that contains spam indicator and the text itself

library(data.table)
library(magrittr)
library(foreach)
source("utils.R")

extractFolder <- tempdir()
untar("lingspam_public.tar.bz2", files="lingspam_public/bare", exdir=extractFolder)
partsFolder <- file.path(extractFolder, "lingspam_public", "bare")
parts <- list.files(partsFolder)
##
emails <- foreach(part = parts, .combine=rbind) %do% {
   files  <- list.files(file.path(partsFolder, part))
   spam <- startsWith(files, "spmsg")
   msgs <- foreach(file = files, .combine=c) %do% {
      msg <- readLines(file.path(partsFolder, part, file)) %>%
         paste(collapse=" ")
   }
   data.table(spam, files, message=msgs)
}

writeTable(emails, "lingspam-emails.csv")
cat("done :-)\n")
