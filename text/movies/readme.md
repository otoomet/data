# Movie-related text data

## imdb-reviews.csv

Collect imdb reviews from
[Large Movie Review
Dataset](https://ai.stanford.edu/~amaas/data/sentiment/) and convert
into a csv.
That dataset is split to test/validation parts (25k each) and each of these
are split into positive/negative reviews
score is coded as _x in part of the file name, e.g.
1234_5.txt is the review text for movie 1234, that has score 5.

Resulting data frame has 3 variables:

* fName: original file name
* score: reivewer's score (1-10)
* text: review text
