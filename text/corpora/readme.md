# Text corpora

## ai-for-thai-best: AI-for-Thai corpus

Received it from Firn.  Contains short texts in thay (articles,
novels, encyclopedia, and news.  Only one part of the corpus, the
other part was not used.

## Coca: Corpus of Contemporary American English

See https://www.english-corpora.org/coca/

### corona

See https://www.english-corpora.org/corona/ . 

Downloaded from https://www.corpusdata.org/formats.asp , 2023-03-14,
the linear text version.

## now: New on the Web

Web-based news articles and magazines, see
https://www.english-corpora.org/now/ .

Downloaded from https://www.corpusdata.org/formats.asp 2023-03-14, the
linear text version.

## 
