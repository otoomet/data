# Text Data

## Corpora: text corpora


## Books: books from Project Gutenberg

Intended for using as text source for various projects


## Embeddings: word embeddings

Various word embeddings

## frequency-corpus

English word frequency, 333k words, downloaded from somewhere.
Contains not just legit EN words but all kinds of things, including
words from other languages, acronyms, mis-spellings, place names,
multiple words written together w/o space etc.


## Sherlock Holmes

Text from Gutenberg Project, ocr-d with
```
convert.sh -l 100 sherlock-holmes.txt --pointsize 13
```

## Trump Tweets

downloaded from https://github.com/ecdedios/ngram-quick-start


## Word Frequencey

Downloaded from https://www.wordfrequency.info/samples.asp (top 60k
lemmas + word forms)


## Scripts:

* **book-page-images.sh**: transform given text file into images of text
  to be used for image categorization
* **image-all-books.sh**: convert all text files in a given dir into
  images, includes AI-for-Thai novels.  Chooses font according to the
  language. 

