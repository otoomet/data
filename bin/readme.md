# Code to create curated data

* `us-elections.R`: loads income and population by county (from BEA),
  county land area, education, population from [US
  Census](https://www.census.gov/library/publications/2011/compendia/usa-counties-2011.html#LND)
  (LND01.xls, EDU02.xls, POP01.xls, POP02.xls, PST01.xls),
  and adds election data (from MIT election lab).  See the folder
  _us-elections_ for the description.
  
