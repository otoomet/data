# Music-related data

## Bands 

Compiled by 柯柳含

4 rows.  Columns:

* **Band**: name
* **Year Formed**
* **Genre**
* **Based In**: city or country

## Songs

Compiled by 柯柳含

30 rows.  Columns: 

* **Song Name**
* **Album/EP**
* **Duration**: m:ss
* **Band**
* **Release Year**
